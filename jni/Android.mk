LOCAL_PATH := $(call my-dir)
GUI3D_PATH := $(LOCAL_PATH)/../../../Gui3d

include $(CLEAR_VARS)
LOCAL_MODULE := bullet281
LOCAL_SRC_FILES := ../libs/armeabi-v7a/libBullet.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := liblua
LOCAL_SRC_FILES := ../libs/armeabi-v7a/liblua.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libRocket
LOCAL_SRC_FILES := ../libs/armeabi-v7a/librocket-debug.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := cpufeats
LOCAL_SRC_FILES := ../libs/armeabi-v7a/libcpufeatures.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := android_glue
LOCAL_SRC_FILES := ../libs/armeabi-v7a/libandroid_native_app_glue.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := MyOgreJNI
LOCAL_LDLIBS := -landroid -lc -lm -ldl -llog -lGLESv2 -lEGL
LOCAL_LDLIBS += -L./../../OgreAndroidSDK/Ogre/lib/armeabi-v7a
LOCAL_LDLIBS += -L./../../OgreAndroidSDK/Dependencies/lib/armeabi-v7a
LOCAL_EXPORT_LDLIBS := -lPlugin_ParticleFXStatic -lPlugin_OctreeSceneManagerStatic
LOCAL_EXPORT_LDLIBS += -lRenderSystem_GLES2Static -lOgreRTShaderSystemStatic
LOCAL_EXPORT_LDLIBS += -lOgreOverlayStatic -lOgreMainStatic
LOCAL_EXPORT_LDLIBS += -lzzip -lz -lFreeImage -lfreetype -lOIS -lmesa -lglsl_optimizer
LOCAL_EXPORT_LDLIBS += -llibsupc++.a
LOCAL_EXPORT_LDLIBS += ./../../OgreAndroidSDK/Dependencies/lib/armeabi-v7a/libstdc++.a
LOCAL_EXPORT_LDLIBS += ./../../OgreAndroidSDK/Samples/GLES2/obj/local/armeabi-v7a/libcpufeatures.a
LOCAL_STATIC_LIBRARIES := android_glue bullet281 liblua
LOCAL_CFLAGS := -fpermissive -Wno-psabi -fexceptions -frtti -x c++ -D___ANDROID___ -DANDROID
LOCAL_CFLAGS += -DZZIP_OMIT_CONFIG_H -DGL_GLEXT_PROTOTYPES=1 -DUSE_RTSHADER_SYSTEM=1
LOCAL_CFLAGS += -I./../../android-ndk-r9d-linux-x86_84/sources/android/native_app_glue
LOCAL_CFLAGS += -I./../../android-ndk-r9d-linux-x86_84/sources/cpufeatures
LOCAL_CFLAGS += -I./../../bullet_2.81/src
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/RenderSystems/GLES2/EGL
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/RenderSystems/GLES2
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/Build
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/OgreMain
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/Components/Overlay
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/Components/RTShaderSystem 
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/PlugIns/ParticleFX
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Ogre/include/PlugIns/OctreeSceneManager 
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Dependencies/include
LOCAL_CFLAGS += -I./../../OgreAndroidSDK/Dependencies/include/OIS
LOCAL_CFLAGS += -I./../../BtOgre/include
LOCAL_SRC_FILES :=	./BtOgre.cpp \
					./Main.cpp
include $(BUILD_SHARED_LIBRARY)
#ifndef __PACKETS_H_INCLUDED__   // if packets.h hasn't been included yet...
#define __PACKETS_H_INCLUDED__   //   #define this so the compiler knows it has been included

#include <android/log.h>
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, "MyOgreGame", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "MyOgreGame", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, "MyOgreGame", __VA_ARGS__)

#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <string>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include "unistd.h"
#include "errno.h"
//#include "strings.h"          ******* REINCLUDE!!
//#include "stdio.h"          ******* REINCLUDE!!

extern unsigned char _PACKET_DATA[256];
extern unsigned int _MAXPACKETSIZE;

struct packet
{
	int packetID;
	std::string accntName;
	char* packetData[18];
};

struct loginPacket
{
	std::string accntName;
	std::string accntPswd;
	//int clientID;
	//int sessionID;
};

struct loginSuccessPacket
{
	int serverID;
	int sessionID;
	int error;
	bool loginSuccess;
};

struct chatPacket
{
	std::string sendStr;
	char* sendChar[sizeof(std::string)];
};

struct stayLiveRequestPacket
{
	bool requestKeepAlive;
	bool requestDisconnect;
	int clientID;
	int serverID;
	std::string timeStamp;
};

struct stayLiveReturnPacket
{
	bool stayAlive;
	int clientID;
	int serverID;
	std::string timestamp;
};

struct positionPacket
{
	std::string areaZone;
	int playerX;
	int playerY;
	int playerZ;
};

struct playerPositions
{
	int playerX;
	int playerY;
	int playerZ;
};

struct areaUpdatePacket
{
	std::map<std::string,playerPositions> playerPositionMap;
};

enum loginErrCodes
{
    BADUSERNAME=1,
    BADPASSWORD,
    ALREADYLOGGEDIN,
    SERVERFULL,
    SERVERBUSY
};

enum packetTypes
{
    LOGIN=1,
    LOGINSUCCESS,
    CHAT,
    STAYLIVE,
    STAYLIVERETURN,
    PLAYERUPDATE,
    POSITIONSUPDATE
};

struct RECVDPACKET
{
    packet rPacket;
    sockaddr_in rAddress;
};

struct PLAYER
{
    std::string playerName;
    std::string accntName;
    bool wantsToStayLive;
    playerPositions playerPosition;
};

struct NETPACKET
{
	unsigned int PACKETTYPE;
	unsigned char dataBuffer[256];
};

#endif

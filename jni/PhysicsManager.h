#include "OgrePlatform.h"
#include "Ogre.h"

#include "BtOgrePg.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "BulletDynamics/Character/btCharacterControllerInterface.h"

#include "MyMotionStates.h"




class PhysicsManager
{
public:
	PhysicsManager(bool showDebug);
	~PhysicsManager(){};

	bool btDebugOn;
	bool mInit;
	float _GRAVITY;

	Ogre::SceneNode* ogreRootNode;
	Ogre::SceneManager* currentSceneMgr;

	btDynamicsWorld *mBtWorld;
	btAxisSweep3 *mBtBroadphaseAxis3;
	//btBroadphaseInterface *mBtBroadphase;
	btDefaultCollisionConfiguration *mBtCollisionConfiguration;
	btCollisionDispatcher *mBtCollisionDispatcher;
	btSequentialImpulseConstraintSolver *mBtConstraintSolver;

	std::deque<btCollisionShape *>  mBtShapes;
	std::deque<btRigidBody *> 		mBtRigBodies;
	std::map<std::string,btRigidBody *> mBtBodies;
	std::map<std::string,Ogre::SceneNode*> mBtActiveObjects;
	BtOgre::DebugDrawer* mBtDebugDrawer;

	void setupDebugDrawer(void);

	bool init(Ogre::SceneManager* sceneMgrToAttachTo);

	float getGravity(void){return (_GRAVITY);};
	void setGravity(float NewGravity){_GRAVITY = NewGravity;};

	void update(Ogre::Real mTimeSinceLast);
};

PhysicsManager::PhysicsManager(bool showDebug) : mInit(false),_GRAVITY(-9.8f),currentSceneMgr(NULL),btDebugOn(showDebug)
{};

void PhysicsManager::update(Ogre::Real mTimeSinceLast)
{
	if(mInit)
	{
		mBtWorld->stepSimulation(mTimeSinceLast);

		if( btDebugOn && mBtDebugDrawer)
		{
			mBtDebugDrawer->step();
		}
	}
};

bool PhysicsManager::init(Ogre::SceneManager* sceneMgrToAttachTo)
{	LOGD("PhysicsManager::init() Started.");

	bool started = false;

	mBtBroadphaseAxis3 = new btAxisSweep3(btVector3(-1000,-1000,-1000), btVector3(1000,1000,1000), 1024);
	mBtCollisionConfiguration = new btDefaultCollisionConfiguration();
	mBtCollisionDispatcher = new btCollisionDispatcher(mBtCollisionConfiguration);
	mBtConstraintSolver = new btSequentialImpulseConstraintSolver();
	mBtWorld = new btDiscreteDynamicsWorld(mBtCollisionDispatcher, mBtBroadphaseAxis3, mBtConstraintSolver, mBtCollisionConfiguration);
	mBtWorld->setGravity(btVector3(0,_GRAVITY,0));

	currentSceneMgr = sceneMgrToAttachTo;
	ogreRootNode = currentSceneMgr->getRootSceneNode();

	if(btDebugOn)
	{setupDebugDrawer();}

	if(started)
	{
		return (true);
	}
	else
	{
		return (false);
	}

	//Bullet initialization.
	//mBtBroadphase = new btDbvtBroadphase();
}

void PhysicsManager::setupDebugDrawer(void)
{	LOGD("PhysicsManager: Started with debug drawer showing.");
	mBtDebugDrawer = new BtOgre::DebugDrawer(ogreRootNode, mBtWorld);
	mBtWorld->setDebugDrawer(mBtDebugDrawer);
	mBtDebugDrawer->setDebugMode(1);
};

#include "Game.h"

MyGame::MyGame(Ogre::RenderWindow* mWin,btDynamicsWorld *mWorld,BtOgre::DebugDrawer* physDbgDrw, MyGui* mGuiSys) : 	mWindow(mWin),
																													mPhysDbgDrw(physDbgDrw),
																													mPhysWorld(mWorld),
																													gui(mGuiSys)
{
	//Register as a Window listener
	Ogre::WindowEventUtilities::addWindowEventListener(mRenderWind, this);
};


bool MyGame::frameStarted(const Ogre::FrameEvent& evt)
{
	return (true);
};

bool MyGame::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	mPhysWorld->stepSimulation(evt.timeSinceLastFrame);	// update Bullet Physics animation
	mDbgDrw->step();						// Step debug drawer to reflect the world changing
	gui->update(evt.timeSinceLastFrame);
	return (true);
};

bool MyGame::frameEnded(const Ogre::FrameEvent& evt)
{
	return (true);
};

#ifndef __GAME_H_INCLUDED__
#define __GAME_H_INCLUDED__
#include "MyOgreGame.h"

MyGui* mGui;

class MyGame: public Ogre::FrameListener, public Ogre::WindowEventListener
{
	public:
		MyGame(Ogre::RenderWindow* mWin,btDynamicsWorld *mPhysWorld,BtOgre::DebugDrawer* mDbgDrw, MyGui* mGuiSys);
		~MyGame(void){Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);};
		bool frameStarted(const Ogre::FrameEvent& evt);
		bool frameRenderingQueued(const Ogre::FrameEvent& evt);
		bool frameEnded(const Ogre::FrameEvent& evt);

		Ogre::RenderWindow* mWindow;
		MyGui* gui;
		btDynamicsWorld *mPhysWorld;
		BtOgre::DebugDrawer* mPhysDbgDrw;
};

#endif

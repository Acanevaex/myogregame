#ifndef __MYOGREGAME_H_INCLUDED__
#define __MYOGREGAME_H_INCLUDED__

//#include "MyGui.h"
#include "android_bridge.h"

static void setDebugDrawer(void)
{	LOGD("setDebugDrawer Started");
	mDbgDrw = new BtOgre::DebugDrawer(mSceneMgr->getRootSceneNode(), mPhysWorld);
	LOGD("initBullet: mdbgDrw created");
	mPhysWorld->setDebugDrawer(mDbgDrw);
	mDbgDrw->setDebugMode(1);
	LOGD("initBullet: mPhysWorld dbgDrw set");
};

static void initBullet(void)
{	LOGD("MYGAME::initBullet: Start.");
	//Bullet initialization.
	mBroadphase = new btAxisSweep3(btVector3(-1000,-1000,-1000), btVector3(1000,1000,1000), 1024);
	mCollisionConfig = new btDefaultCollisionConfiguration();
	mCollisionDispatcher = new btCollisionDispatcher(mCollisionConfig);
	mConstraintSolver = new btSequentialImpulseConstraintSolver();
	mPhysWorld = new btDiscreteDynamicsWorld(mCollisionDispatcher, mBroadphase, mConstraintSolver, mCollisionConfig);
	mPhysWorld->setGravity(btVector3(0,-9.8,0));
	LOGD("MYGAME::initBullet: Done.");

	setDebugDrawer();
};

static void myInit(void)
{	LOGD("MYGAME::myInit Started");

	Ogre::Entity* pEntity;
	Ogre::SceneNode* mNode;
	btCollisionShape *mTempShape;
	btDefaultMotionState* mTempState;
	BtOgre::RigidBodyState* mRigidState;
	btRigidBody *mTempBody;

	if( mSceneMgr->hasEntity("Ground") )
	{// make Collision shape, state, & body for 'Ground'
		LOGD("mSceneMgr found 'Ground' entity");

		pEntity = mSceneMgr->getEntity("Ground");
		BtOgre::StaticMeshToShapeConverter mConverter2(pEntity);
		mTempShape = mConverter2.createBox();//.createTrimesh();
		mTempState = new btDefaultMotionState(
											btTransform(btQuaternion(0,0,0,1),btVector3(0,0,0)));
		mTempBody = new btRigidBody(0, mTempState, mTempShape, btVector3(0,0,0));
		mTempBody->getWorldTransform();
		mTempBody->setFriction(0.5f);
		mTempBody->setRestitution(0.01f);
		mTempBody->setDamping(1.0f,1.0f);
		mPhysWorld->addRigidBody(mTempBody);
		mShapes.push_back(mTempShape);
		mRigBodies.push_back(mTempBody);

		Ogre::SceneNode* mNode = pEntity->getParentSceneNode();
		activeObjects.insert( std::pair<std::string,Ogre::SceneNode*>(pEntity->getName().c_str(),mNode) );
	}
	else{ LOGD("mSceneMgr did not find 'Ground' entity");}

	LOGD("MYGAME::myInit Finished");
};

void destroyBullet(void)
{
	//cleanup Bullet Dynamics
	//cleanup in the reverse order of creation/initialization
	//remove the RigidBodies from the dynamics world and delete them
	for ( int i = mPhysWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = mPhysWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}

		mPhysWorld->removeCollisionObject( obj );
		delete obj;
	}

	// delete bullet collision shapes
	std::deque<btCollisionShape *>::iterator itShape = mShapes.begin();
	for( itShape=mShapes.begin(); itShape!=mShapes.end(); ++itShape )
	{
		delete *itShape;
		++itShape;
	}

	// delete bullet collision bodies
	std::deque<btRigidBody *>::iterator itBody = mRigBodies.begin();
	for( itBody=mRigBodies.begin(); itBody!=mRigBodies.end(); ++itBody )
	{
		delete *itBody;
		++itBody;
	}

	/*std::map<std::string,btRigidBody *>::iterator itChar = mBodies.begin();
	for (itChar=mBodies.begin(); itChar!=mBodies.end(); ++itChar)
	{
		delete *itChar->second;
		++itChar;
	}*/

	mShapes.clear();
	mRigBodies.clear();
	mBodies.clear();

	delete mPhysWorld;
	delete mConstraintSolver;
	delete mCollisionDispatcher;
	delete mCollisionConfig;

};

#endif

#include "OIS.h"
#include "OgrePlatform.h"
#include "Ogre.h"

/*=============================================================================
     | Android input handling
     =============================================================================*/
class AndroidMultiTouch : public OIS::MultiTouch
{
public:
	std::string mAppName;
	AndroidMultiTouch(std::string appName):OIS::MultiTouch("DWM", false, 0, 0),mAppName(appName.c_str()){}

	/** @copydoc Object::setBuffered */
	virtual void setBuffered(bool buffered){}

	/** @copydoc Object::capture */
	virtual void capture(){}

	/** @copydoc Object::queryInterface */
	virtual OIS::Interface* queryInterface(OIS::Interface::IType type) {return (0);}

	/** @copydoc Object::_initialize */
	virtual void _initialize(){}

	OIS::MultiTouchState &getMultiTouchState(int i){
		while(i >= mStates.size()){
			Ogre::RenderWindow* pRenderWnd = static_cast<Ogre::RenderWindow*>(Ogre::Root::getSingleton().getRenderTarget(mAppName));
			if(pRenderWnd)
			{
				OIS::MultiTouchState state;
				state.width = pRenderWnd->getWidth();
				state.height = pRenderWnd->getHeight();
				mStates.push_back(state);
			}
		}
		return (mStates[i]);
	}
};

class AndroidKeyboard : public OIS::Keyboard
{
public:
	AndroidKeyboard():OIS::Keyboard("DWM", false, 1, 0){}

	/** @copydoc Object::setBuffered */
	virtual void setBuffered(bool buffered){}

	/** @copydoc Object::capture */
	virtual void capture(){}

	/** @copydoc Object::queryInterface */
	virtual OIS::Interface* queryInterface(OIS::Interface::IType type) {return (0);}

	/** @copydoc Object::_initialize */
	virtual void _initialize(){}

	virtual bool isKeyDown( OIS::KeyCode key ) const{
		return (false);
	}

	virtual const std::string& getAsString( OIS::KeyCode kc ){
		static std::string defstr = "";
		return (defstr);
	}

	virtual void copyKeyStates( char keys[256] ) const{

	}
};

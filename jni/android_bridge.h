//#include "MyLogger.h"
#include "android_inputhelp.h"
#include "PhysicsManager.h"
#include "MyGui.h"

class AndroidGame : public OIS::MultiTouchListener, public Ogre::FrameListener, public Ogre::WindowEventListener
{
public:
	AndroidGame(bool showDebug) : mFirstRun(true),mIsStarted(false),mLastRun(false),mNumCams(0),mNumLights(0),mStartOverlay(NULL),guiStarted(false),physicsStarted(false){};
	~AndroidGame(){ Ogre::WindowEventUtilities::removeWindowEventListener(mGameWindow, this); };

	AAssetManager* gameAssetMgr;
	AndroidMultiTouch* mTouch;
	AndroidKeyboard* mKeyboard;
	MyGui* gameGui;
	struct android_app* appState;
	Ogre::SceneManager* mSceneMan;
	Ogre::RenderWindow* mGameWindow;
	Ogre::Root* mOgreRoot;
	Ogre::OverlaySystem* mOgreOverlaySys;
	Ogre::OverlayManager* mOgreOverlayMgr;
	Ogre::Overlay* mStartOverlay;
	Ogre::Camera* mCurrGameCam;
	Ogre::SceneNode* mCurrCamNode;
	Ogre::Viewport* mCurrViewport;
	PhysicsManager* mPhysicMgr;

	bool debugOn;
	bool guiStarted;
	bool physicsStarted;
	bool mFirstRun;
	bool mIsStarted;
	bool mLastRun;

	int mNumCams;
	int mNumLights;

	std::deque<Ogre::OverlayElement*> activeElements;
	std::map<Ogre::OverlayElement*,Ogre::Vector2> elementMap;

	bool init(AndroidMultiTouch* touch, AndroidKeyboard* keyboard, struct android_app* extApp);
	bool loadResources();
	Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName);
	void startUp(Ogre::RenderWindow* ogreWindow, Ogre::Root* mainRoot, Ogre::OverlaySystem* overlaySys);
	void setupCamView();
	void setupSceneMgr();
	void showStartScreen();
	void shutdown();

	bool isFirstRun();
	void setFirstRun(bool flag);
	bool isLastRun();
	void setLastRun(bool flag);

	bool frameStarted(const Ogre::FrameEvent& evt);
	bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	bool frameEnded(const Ogre::FrameEvent& evt);

	virtual bool keyPressed(const OIS::KeyEvent& evt);
	virtual bool keyReleased(const OIS::KeyEvent& evt);
	bool touchCancelled(const OIS::MultiTouchEvent& evt);
	bool touchMoved(const OIS::MultiTouchEvent& evt);
	bool touchPressed(const OIS::MultiTouchEvent& evt);
	bool touchReleased(const OIS::MultiTouchEvent& evt);

	//void windowMoved(Ogre::RenderWindow* renderWindow);
	void windowResized(Ogre::RenderWindow* renderWindow);
	//bool windowClosing(Ogre::RenderWindow* renderWindow);

	void transformInputState(OIS::MultiTouchState &state);
	void update(void);
};

/*=============================================================================
     | Android input injection
     =============================================================================*/
class AndroidInputInjector
{
private:
	AndroidGame* mGame;
	AndroidMultiTouch* mTouch;
	AndroidKeyboard* mKeyboard;

public:

	AndroidInputInjector(AndroidMultiTouch* touch, AndroidKeyboard* keyboard, AndroidGame* gameToHandle)
	:  mTouch(touch), mKeyboard(keyboard), mGame(gameToHandle) {}

	void injectKeyEvent(int action, int32_t keyCode)
	{
		if(keyCode == AKEYCODE_BACK)
		{
			OIS::KeyEvent evt(mKeyboard, OIS::KC_ESCAPE, 0);
			if(action == 0)
			{
				mGame->keyPressed(evt);
			}
			else
			{
				mGame->keyReleased(evt);
			}
		}
	}

	void injectTouchEvent(int action, float x, float y, int pointerId = 0)
	{
		OIS::MultiTouchState &state = mTouch->getMultiTouchState(pointerId);

		switch(action)
		{
		case 0:
			state.touchType = OIS::MT_Pressed;
			break;
		case 1:
			state.touchType = OIS::MT_Released;
			break;
		case 2:
			state.touchType = OIS::MT_Moved;
			break;
		case 3:
			state.touchType = OIS::MT_Cancelled;
			break;
		default:
			state.touchType = OIS::MT_None;
			break;
		}

		if(state.touchType != OIS::MT_None)
		{
			int last = state.X.abs;
			state.X.abs =  (int)x;
			state.X.rel = state.X.abs - last;

			last = state.Y.abs;
			state.Y.abs = (int)y;
			state.Y.rel = state.Y.abs - last;

			state.Z.abs = 0;
			state.Z.rel = 0;

			OIS::MultiTouchEvent evt(mTouch, state);

			switch(state.touchType)
			{
			case OIS::MT_Pressed:
				mGame->touchPressed(evt);
				break;
			case OIS::MT_Released:
				mGame->touchReleased(evt);
				break;
			case OIS::MT_Moved:
				mGame->touchMoved(evt);
				break;
			case OIS::MT_Cancelled:
				mGame->touchCancelled(evt);
				break;
			default:
				break;
			}
		}
	}
};

Ogre::DataStreamPtr AndroidGame::openAPKFile(const Ogre::String& fileName)
{
	Ogre::DataStreamPtr stream;
    AAsset* asset = AAssetManager_open(gameAssetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
    if(asset)
    {
		off_t length = AAsset_getLength(asset);
        void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
        memcpy(membuf, AAsset_getBuffer(asset), length);
        AAsset_close(asset);

        stream = Ogre::DataStreamPtr(new Ogre::MemoryDataStream(membuf, length, true, true));
    }
    return (stream);
};

bool AndroidGame::loadResources()
{	//LOGD("START OF INIT");

	Ogre::ConfigFile cf;
	cf.load(openAPKFile("resources.cfg"));
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	Ogre::String sec, type, arch;
	while(seci.hasMoreElements())
	{
		sec = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for(i = settings->begin(); i != settings->end(); i++)
		{
			type = i->first;
			arch = i ->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(arch,type,sec);
		}
	}
	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingletonPtr()->setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("General");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("Fonts");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("GUI");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("PACKS");

	//Ogre::ResourceGroupManager::getSingletonPtr()->initialiseAllResourceGroups();


	if( Ogre::RTShader::ShaderGenerator::initialize() )
	{	// ONLY FOR GLES2
		Ogre::RTShader::ShaderGenerator::getSingletonPtr()->setTargetLanguage("glsles");
		mMatListener = new Ogre::ShaderGeneratorTechniqueResolverListener();
		Ogre::MaterialManager::getSingleton().addListener(mMatListener);
		return (true);
	}
	else
	{
		LOGD("INIT:: Failed to Initialize RTShader::ShaderGen.");
		return (false);
	}
};

bool AndroidGame::init(AndroidMultiTouch* touch, AndroidKeyboard* keyboard, struct android_app* extApp)
{
	if(mIsStarted)
	{
		return (true);
	}
	else
	{
		appState = extApp;
		mTouch = touch;
		mKeyboard = keyboard;
		gameAssetMgr = appState->activity->assetManager;

		mIsStarted = true;
		return (true);
	}
};

void AndroidGame::showStartScreen()
{
	//Make overlayManager create start screen with   _START_  button
	mStartOverlay = mOgreOverlayMgr->getByName("GuiOverlays/StartScreen");
	Ogre::OverlayElement* startPanel = mOgreOverlayMgr->getOverlayElement("GUI_MAIN/StartScreen");

	if(mStartOverlay)
	{
		if(startPanel)
		{
		activeElements.push_back(startPanel);
			startPanel->setDimensions(mCurrViewport->getActualWidth(),mCurrViewport->getActualHeight());
			elementMap[startPanel] = Ogre::Vector2(startPanel->getHeight(),startPanel->getWidth());
		}
		Ogre::OverlayElement* mButtonCaption = mOgreOverlayMgr->getOverlayElement("StartButton/Caption");
		LOGD("^^^*** Found OverlayMgr->getByName(GuiOverlays/StartScreen), now showing!!!");
		//mStartOverlay->
		mStartOverlay->show();

		if(mButtonCaption)
		{
			mButtonCaption->setCaption("START GAME");
		}
	}

};

void AndroidGame::startUp(Ogre::RenderWindow* ogreWindow, Ogre::Root* mainRoot, Ogre::OverlaySystem* overlaySys)
{
	if(mIsStarted)
	{
		mGameWindow = ogreWindow;
		mOgreRoot = mainRoot;
		mOgreOverlaySys = overlaySys;
		if( loadResources() )
		{
			setupSceneMgr();
			setupCamView();

			showStartScreen();

			gameGui = new MyGui(appState,debugOn,mSceneMan,mOgreOverlaySys);
			gameGui->init(mCurrCamNode,mCurrViewport);
			guiStarted = true;
		}
	}
};

void AndroidGame::setupSceneMgr()
{
	mSceneMan = mOgreRoot->createSceneManager(Ogre::ST_GENERIC,"MainSceneManager");
	Ogre::RTShader::ShaderGenerator::getSingletonPtr()->addSceneManager(mSceneMan);
	mSceneMan->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED);
	mSceneMan->addRenderQueueListener(mOgreOverlaySys);
	mSceneMan->setAmbientLight(Ogre::ColourValue(0.5f,0.5f,0.5f,0.5f));
	mOgreOverlayMgr = Ogre::OverlayManager::getSingletonPtr();

	// Register as a WindowListener
	Ogre::WindowEventUtilities::addWindowEventListener(mGameWindow, this);
	// Register as a FrameListener
	mOgreRoot->addFrameListener(this);
};

void AndroidGame::setupCamView()
{
	Ogre::SceneNode* pNode;
	Ogre::Entity* pEntity;
	Ogre::Light* pLight;

	// Create Game Camera and accompanying viewport
	mNumCams++;
	mCurrGameCam = mSceneMan->createCamera(Ogre::StringConverter::toString("GameCam"+mNumCams)); //GameCam1 ***should be***
	mCurrGameCam->setNearClipDistance(0.1f);
	mCurrGameCam->setFarClipDistance(3000.0f);
	mCurrGameCam->setAutoAspectRatio(true);
	mCurrViewport = mGameWindow->addViewport(mCurrGameCam);

	//mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0,.10));
	mCurrViewport->setOverlaysEnabled(true);
	mCurrViewport->setMaterialScheme(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
	Ogre::RTShader::ShaderGenerator::getSingletonPtr()->invalidateScheme(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
	mCurrViewport->setShadowsEnabled(true);

	// Create a sceneNode for camera named: CameraNode
	stringstream ss;
	ss << "GameCamNode" << mNumCams;
	mCurrCamNode = mSceneMan->getRootSceneNode()->createChildSceneNode(ss.str().c_str()); //GameCamNode1 ***should be***
	mCurrCamNode->attachObject(mCurrGameCam);
	mCurrCamNode->setPosition(0.0f,4.0f,30.0f);
	mCurrGameCam->lookAt(0.f,2.5f,0.f);

	mNumLights++;
	pLight = mSceneMan->createLight(Ogre::StringConverter::toString("Light"+mNumLights));
	pLight->setType(Ogre::Light::LT_POINT);
	ss.str("");
	ss.clear();
	ss << "LightNode" << mNumLights;
	pNode = mSceneMan->getRootSceneNode()->createChildSceneNode(ss.str().c_str());
	pNode->attachObject(pLight);
	pNode->setPosition(0.0,20.0f,0.0);


	// Create plane mesh, name: groundPlane, in ResGroup General.
	Ogre::MeshPtr mMesh = Ogre::MeshManager::getSingleton().createPlane("groundPlane","General",Plane(Vector3::UNIT_Y,0),500,500,
			5, 5, true, 1, 10, 10, Vector3::UNIT_Z);
	mMesh->buildEdgeList();
	pEntity = mSceneMan->createEntity("TempGround", "groundPlane");
	Ogre::MaterialPtr mMat = Ogre::MaterialManager::getSingleton().getByName("MyMats/BumpyMetal");
	mMat->setAmbient(0.5f,0.5f,0.5f);
	mMat->setDiffuse(0.5f,0.5f,0.5f,1);
	mMat->setLightingEnabled(true);
	pEntity->setMaterialName("MyMats/BumpyMetal");

	pNode = mSceneMan->getRootSceneNode()->createChildSceneNode("TempGroundNode");
	pNode->attachObject(pEntity);
	pNode->setScale(1.0f,0.05f,1.0f);
	pNode->setPosition(0,0,0);

	mSceneMan->setSkyDome(true, "MyMats/CloudySky", 6, 6, 1000, false);

//	pEntity = mSceneMan->createEntity("TempStartEntity", "cube.mesh");
//	pNode = mSceneMan->getRootSceneNode()->createChildSceneNode("TempStartNode");
//	pNode->attachObject(pEntity);
//	pNode->setPosition(0.f,0.f,0.f);
//	pNode->setScale(20.0f,0.5f,20.0f);
};

void AndroidGame::shutdown()
{

};

bool AndroidGame::keyPressed(const OIS::KeyEvent& evt)
{
	return (true);
}

bool AndroidGame::keyReleased(const OIS::KeyEvent& evt)
{
	return (true);
}

bool AndroidGame::touchMoved(const OIS::MultiTouchEvent& evt)
{
	stringstream ss;
	OIS::MultiTouchState state = evt.state;
	OIS::MultiTouchEvent orientedEvt((OIS::Object*)evt.device, state);

	ss << "state.X.abs: " << state.X.abs << "state.Y.abs: " << state.Y.abs;
	LOGD(ss.str().c_str());
	ss.str("");
	ss.clear();
	return (true);
}


bool AndroidGame::touchPressed(const OIS::MultiTouchEvent& evt)
{
	stringstream ss;
	OIS::MultiTouchState state = evt.state;
	OIS::MultiTouchEvent orientedEvt((OIS::Object*)evt.device, state);

	ss << "state.X.abs: " << state.X.abs << "state.Y.abs: " << state.Y.abs;
	LOGD(ss.str().c_str());
	ss.str("");
	ss.clear();
	return (true);
}

bool AndroidGame::touchReleased(const OIS::MultiTouchEvent& evt)
{
	stringstream ss;
	OIS::MultiTouchState state = evt.state;
	OIS::MultiTouchEvent orientedEvt((OIS::Object*)evt.device, state);

	ss << "state.X.abs: " << state.X.abs << "state.Y.abs: " << state.Y.abs;
	LOGD(ss.str().c_str());
	ss.str("");
	ss.clear();

	if(mStartOverlay)
	{
		Ogre::OverlayElement* mElement;
		mElement = mOgreOverlayMgr->getOverlayElement("StartScreen/StartButton");//mStartOverlay->getChild("Button_Start"));

		if(mElement)
		{
			LOGD("^*^*^ FOUND mOgreOverlayMgr->getOverlayElement(StartScreen/StartButton)");
			if( gameGui->isCursorOver(mElement,Vector2(state.X.abs,state.Y.abs)) )
			{
				Ogre::BorderPanelOverlayElement* mBorderPanel = (Ogre::BorderPanelOverlayElement*)mElement;
				//mBorderPanel->
				LOGD("Cursor was clicked over StartScreen/StartButton");
			}
		}
	}

	return (true);
}

bool AndroidGame::touchCancelled(const OIS::MultiTouchEvent& evt)
{
	stringstream ss;
	OIS::MultiTouchState state = evt.state;
	OIS::MultiTouchEvent orientedEvt((OIS::Object*)evt.device, state);

	ss << "state.X.abs: " << state.X.abs << "state.Y.abs: " << state.Y.abs;
	LOGD(ss.str().c_str());
	ss.str("");
	ss.clear();;
	return (true);
}

bool AndroidGame::frameStarted(const Ogre::FrameEvent& evt)
{
	return (true);
};

bool AndroidGame::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if( mIsStarted )
	{
		if( physicsStarted )
		{
			mPhysicMgr->update(evt.timeSinceLastFrame);
		}

		if( guiStarted )
		{
			gameGui->update(evt.timeSinceLastFrame);
		}
	}
	return (true);
};

bool AndroidGame::frameEnded(const Ogre::FrameEvent& evt)
{
	return (true);
};

void AndroidGame::windowResized(Ogre::RenderWindow* renderWindow)
{

};

void AndroidGame::transformInputState(OIS::MultiTouchState &state)
{
	int w = mCurrViewport->getActualWidth();
	int h = mCurrViewport->getActualHeight();
	int absX = state.X.abs;
	int absY = state.Y.abs;
	int relX = state.X.rel;
	int relY = state.Y.rel;

	switch (mCurrViewport->getOrientationMode())
	{
	case Ogre::OR_DEGREE_0:
		break;
	case Ogre::OR_DEGREE_90:
		state.X.abs = w - absY;
		state.Y.abs = absX;
		state.X.rel = -relY;
		state.Y.rel = relX;
		break;
	case Ogre::OR_DEGREE_180:
		state.X.abs = w - absX;
		state.Y.abs = h - absY;
		state.X.rel = -relX;
		state.Y.rel = -relY;
		break;
	case Ogre::OR_DEGREE_270:
		state.X.abs = absY;
		state.Y.abs = h - absX;
		state.X.rel = relY;
		state.Y.rel = -relX;
		break;
	}
};

bool AndroidGame::isFirstRun() { return (mFirstRun); }
void AndroidGame::setFirstRun(bool flag) { mFirstRun = flag; }
bool AndroidGame::isLastRun() { return (mLastRun); }
void AndroidGame::setLastRun(bool flag) { mLastRun = flag; }

class AndroidBridge
{
public:
	static void init(struct android_app* state)
	{
		if(mInit)
		{return;}

		state->onAppCmd = &AndroidBridge::handleCmd;
		state->onInputEvent = &AndroidBridge::handleInput;

		gRoot = new Ogre::Root();
		mStaticPluginLoader = new Ogre::StaticPluginLoader();
		mStaticPluginLoader->load();
		gRoot->setRenderSystem(gRoot->getAvailableRenderers().at(0));
		gRoot->initialise(false);
		mOverlaySystem = OGRE_NEW Ogre::OverlaySystem();

		mInit = true;
	}

	static void shutdown()
	{
		if(!mInit)
		{return;}

		mInit = false;

		if(mGame)
		{
			mGame->shutdown();
			OGRE_DELETE mGame;
			mGame = NULL;
		}

		OGRE_DELETE mRoot;
		mRoot = NULL;
		mRenderWnd = NULL;

		delete mTouch;
		mTouch = NULL;

		delete mKeyboard;
		mKeyboard = NULL;

		delete mInputHandler;
		mInputHandler = NULL;

		mStaticPluginLoader->unload();
		delete mStaticPluginLoader;
		mStaticPluginLoader = NULL;
	}

	static int32_t handleInput(struct android_app* app, AInputEvent* event)
	{
		if (mInputHandler)
		{
			if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION)
			{
				int action = (int)(AMOTION_EVENT_ACTION_MASK & AMotionEvent_getAction(event));

				if(action == 0)
				{mInputHandler->injectTouchEvent(2, AMotionEvent_getRawX(event, 0), AMotionEvent_getRawY(event, 0) );}

				mInputHandler->injectTouchEvent(action, AMotionEvent_getRawX(event, 0), AMotionEvent_getRawY(event, 0) );
			}
			else
			{
				mInputHandler->injectKeyEvent(AKeyEvent_getAction(event), AKeyEvent_getKeyCode(event));
			}

			return (1);
		}
		return (0);
	}

	static void handleCmd(struct android_app* app, int32_t cmd)
	{
		switch (cmd)
		{
		case APP_CMD_SAVE_STATE:
			break;
		case APP_CMD_INIT_WINDOW:
			if (app->window && gRoot && mInit)
			{
				AConfiguration* config = AConfiguration_new();
				AConfiguration_fromAssetManager(config, app->activity->assetManager);

				if (!mRenderWnd)
				{
					Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKFileSystemArchiveFactory(app->activity->assetManager) );
					Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKZipArchiveFactory(app->activity->assetManager) );

					Ogre::NameValuePairList opt;
					opt["externalWindowHandle"] = Ogre::StringConverter::toString((int)app->window);
					opt["androidConfig"] = Ogre::StringConverter::toString((int)config);

					String mAppName = "GameWindow";
					mRenderWnd = Ogre::Root::getSingleton().createRenderWindow(mAppName, 0, 0, false, &opt);

					if(!mTouch)
					{mTouch = new AndroidMultiTouch(mAppName);}

					if(!mKeyboard)
					{mKeyboard = new AndroidKeyboard();}

					if(!mGame)
					{
						mGame = OGRE_NEW AndroidGame(true);
						if( mGame->init(mTouch,mKeyboard,app) )
						{
							mGame->startUp(mRenderWnd,gRoot,mOverlaySystem);
							mInputHandler = new AndroidInputInjector(mTouch, mKeyboard, mGame);
						}
					}
				}
				else
				{
					static_cast<AndroidEGLWindow*>(mRenderWnd)->_createInternalResources(app->window, config);
				}

				AConfiguration_delete(config);
			}
			break;
		case APP_CMD_TERM_WINDOW:
			if(mRoot && mRenderWnd)
				static_cast<AndroidEGLWindow*>(mRenderWnd)->_destroyInternalResources();
			break;
		case APP_CMD_GAINED_FOCUS:
			break;
		case APP_CMD_LOST_FOCUS:
			break;
		case APP_CMD_CONFIG_CHANGED:
			break;
		}
	}

	static void go(struct android_app* app)
	{
		int ident, events;
		struct android_poll_source* source;

		while (true)
		{
			while ((ident = ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0)
			{
				if (source != NULL)
					source->process(app, source);

				if (app->destroyRequested != 0)
					return;
			}

			if(mRenderWnd != NULL && mRenderWnd->isActive())
			{
				mRenderWnd->windowMovedOrResized();
				gRoot->renderOneFrame();
			}
		}
	}

	static Ogre::RenderWindow* getRenderWindow(){return (mRenderWnd);}
private:
	static AndroidGame* mGame;
	static AndroidMultiTouch* mTouch;
	static AndroidKeyboard* mKeyboard;
	static AndroidInputInjector* mInputHandler;
	static Ogre::StaticPluginLoader* mStaticPluginLoader;
	static Ogre::RenderWindow* mRenderWnd;
	static Ogre::Root* gRoot;
	static Ogre::OverlaySystem* mOverlaySystem;
	static bool mInit;
};

#ifndef __ANDROID_SOCKET_H_INCLUDED__   // if whatever.h hasnt been included yet
#define __ANDROID_SOCKET_H_INCLUDED__   //   #define this so the compiler knows it has been included

// Net includes. Basically the same as using *Nix sockets.

#include "packets.h"

void clientUpdateLoop(void * arg)
{

};

void clientListenLoop(void * arg)
{

};

class AndNet
{
	public:
		struct sockaddr_in serverAddr;
		struct hostent *hstPtr;
		int socketHandle;
		char *serverName;
		int portnumber;
		int len;
		std::string UserName;

		AndNet(void);
		AndNet(char *Server,int port,std::string userName);
		~AndNet(void){close(socketHandle);};

		int init(void);
		int recvPackets(void);
		int testSend(void);
};

AndNet::AndNet(void) : UserName("DefaultClientName"),
						serverName("192.168.1.233"),
						portnumber(35001),
						len(sizeof(struct sockaddr_in))
{
	bzero(&serverAddr, sizeof(sockaddr_in));
};

AndNet::AndNet(char *Server,int port,std::string userName) : serverName(Server),
															portnumber(port),
															UserName(userName),
															len(sizeof(struct sockaddr_in))
{
	bzero(&serverAddr, sizeof(sockaddr_in));
};

int AndNet::init(void)
{

	if( (socketHandle = socket(AF_INET,SOCK_DGRAM,IPPROTO_IP)) < 0 )
	{
		close(socketHandle);
		return (false);
	}
	else
	{
		if( (hstPtr = gethostbyname(serverName)) == NULL )
		{
			LOGI("System DNS name resolution not configured properly.");
			LOGI("Error number: %f", ECONNREFUSED);
			return (false);
		}
		else
		{
			memcpy((char *)&serverAddr.sin_addr,hstPtr->h_addr,hstPtr->h_length);
			serverAddr.sin_family = AF_INET;
			serverAddr.sin_port = htons((u_short)portnumber);
		}
	}

	return (true);
};

int AndNet::testSend(void)
{
	int sent = 0;
	std::stringstream ss;

	packet tPacket;
	loginPacket tIdPacket;

	LOGI("Initialised packets");

	tPacket.packetID = 1;
	tPacket.accntName = "USER";
	tIdPacket.accntName = tPacket.accntName;
	tIdPacket.accntPswd = "PWD";

	memcpy(&tPacket.packetData,&tIdPacket,sizeof(loginPacket));
	ss << "tpacket.packetID:" << tPacket.packetID << "\naccntName&passwrd:"
						<< tPacket.accntName << "  " << tIdPacket.accntPswd << "\n";

	sent = sendto(socketHandle,(char*)&tPacket,sizeof(packet),0,(struct sockaddr *)&serverAddr,len);

	//int netInt = 4;
	//sent = sendto(socketHandle,(char*)&netInt,sizeof(int),0,(struct sockaddr *)&serverAddr,len);

	if( sent > 0 )
	{
		ss << "Send successful!, sent:" << sent << " bytes of data to server at: " << serverName;
		LOGI(ss.str().c_str());
		ss.str();
		ss.clear();
	}
	else
	{
		LOGI("Send unsuccessful with sent= %f",sent);
	}

	return (sent);
};

int AndNet::recvPackets(void)
{
	packet recvdPacket;
	struct sockaddr_in stReceiveAddr;

	int iLength = (int) sizeof(struct sockaddr_in);
	memset((void*) &stReceiveAddr, 0, iLength);
	int fromlen = sizeof(struct sockaddr_in);

	int rc = recvfrom(socketHandle,(char*)&recvdPacket,sizeof(packet), 0,(struct sockaddr *)&stReceiveAddr, &fromlen);
	if( rc == 0 )
	{
		LOGI("NET:ERROR: Socket Closed.");
		return (rc);
	}
	else if (rc == -1)
	{
		//cerr << "ERROR! Socket error" << endl;
		close(socketHandle);
		return (rc);
	}

	char* pcIpAddress = inet_ntoa(stReceiveAddr.sin_addr);
	int shPort = ntohs(stReceiveAddr.sin_port);

	//cout << "Socket Received: " << rc << " bytes from "
	  // << pcIpAddress << ":" << shPort << endl;

	return (rc);
};

#endif

#ifndef __MYGUI_H_INCLUDED__
#define __MYGUI_H_INCLUDED__

#include "MyLogger.h"

enum SCREENORIENTATION{LANDSCAPE=1,PORTRAIT=2};

struct ScreenSettings
{
	int width;
	int height;
	float aspectRatio;
};

struct OriginalScreenSettings
{
	int screenWidth;
	int screenHeight;
	SCREENORIENTATION originalOrientation;
};

enum GUI_MODE{STARTSCREEN=1,CHARACTERCONTROL=2};

class ScreenOrientMode
{
	public:
		SCREENORIENTATION originalOrientation;
		ScreenSettings landscape,portrait;

		ScreenOrientMode(SCREENORIENTATION orientation, int width, int height)
		{
			originalOrientation = orientation;
			if(orientation == LANDSCAPE)
			{
				landscape.width = width;
				landscape.height = height;
				landscape.aspectRatio = (float)width/height;
			}
			else if( orientation == PORTRAIT )
			{
				portrait.width = width;
				portrait.height = height;
				portrait.aspectRatio = (float)width/height;
			}
		};

		~ScreenOrientMode(){};
};

SCREENORIENTATION findCurrentScreenOrientation(void)
{
	AConfiguration* config = AConfiguration_new();
	AConfiguration_fromAssetManager(config, statApp->activity->assetManager);
	int32_t orientation = AConfiguration_getOrientation(config);
	SCREENORIENTATION currentScreenOrientation;
	if(orientation == ACONFIGURATION_ORIENTATION_LAND)
	{
		currentScreenOrientation = LANDSCAPE;
	}
	else if(orientation == ACONFIGURATION_ORIENTATION_PORT)
	{
		currentScreenOrientation = PORTRAIT;
	}

	return (currentScreenOrientation);
};

class MyGui
{
public:
	MyGui(void){};
	MyGui( struct android_app* app, bool showDebug, Ogre::SceneManager* ogreSceneManager, Ogre::OverlaySystem* mGuiSys);
	~MyGui();

	struct android_app* mApp;

	ScreenOrientMode* mOrientMode;

	OriginalScreenSettings originalScreen;
	SCREENORIENTATION originalOrientation,currentOrientation;
	MyLogger guiLog;

	Ogre::Overlay* guiOverlay;
	Ogre::Overlay* mainScreen;
	Ogre::TextAreaOverlayElement* mDbgTxt1;

	Ogre::OverlayManager* guiMgr;
	Ogre::OverlaySystem* guiSys;
	Ogre::SceneManager* ogreMgr;
	Ogre::Viewport* guiVP;
	Ogre::Camera* mCurrCamera;
	Ogre::SceneNode* currCamNode;
	Ogre::SceneNode* currPlayerNode;
	Ogre::Real mMove,mTimeUntilNextToggle;
	Ogre::Radian turnSpd;
	Ogre::Vector3 transVector;

	int scnWidth,scnHeight;
	bool dbgOn,controllableCharacter;
	//MyKinematicState mCurrPlayer; // TODO: setup controlled character to get derived direction to fire from the front
	bool playerCreated;

	int numObjs;
	long numBoxesFired;

	bool isStarted;

	std::map<Ogre::OverlayContainer*,Ogre::Overlay*> activeGuiPanels;
	std::map<Ogre::OverlayContainer*,Ogre::Vector2> panelOriginalPositions;

	void addControls(void);
	void addDebug(void);
	void addStartScreen(void);
	void destroyGui(void);
	void destroyProgressBar(Ogre::OverlayContainer* progressBarToDestroy);
	SCREENORIENTATION findCurrentScreenOrientation(void);
	void fireBox(Ogre::SceneNode* nodeRequestingFire, Vector3 startPos);
	Ogre::SceneNode* getCurrentPlayerNode();
	void handlePointerTouchDown(float touchX,float touchY);
	void init(Ogre::SceneNode* currentCamNode, Ogre::Viewport* currViewport);
	bool isCursorOver(Ogre::OverlayElement* element, const Ogre::Vector2& cursorPos);//, Ogre::Real voidBorder = 0);
	Ogre::OverlayContainer* makeProgressBar(float percent);
	void screenResized(SCREENORIENTATION LANDSCAPE_OR_PORTRAIT);
	void screenSettingInit(void);
	void screenTouchUpdate(void);
	void setPlayerNode(Ogre::SceneNode* mPlayerNode);
	bool setProgressBarProgress(Ogre::OverlayContainer* progressBarToProgress,float percent);
	void setToLandscapeMode(void);
	void setToPortraitMode(void);
	void setupCharacter(void);
	void update(Ogre::Real timeSinceLast);
	void updateDebug(void);

	AndNet netClient;
	Ogre::OverlayContainer* netPanel,* testSendButton;
	void makeNetControls(void);


	Ogre::OverlayContainer* statsPanel,* mControlsPanel,* mControls,* mControlLft,* mControlRgt,* mControlUp,* mControlDn,* mMakeCharButton;
	Ogre::OverlayContainer* fireButton1;
	Ogre::TextAreaOverlayElement* fpsText,* batchesText,* trianglesText,* mControlsText;
	Ogre::TextAreaOverlayElement* debugText1,* debugText2,* debugText3,* debugText4,* debugText5;


};

MyGui::MyGui( struct android_app* app, bool showDebug, Ogre::SceneManager* ogreSceneManager, Ogre::OverlaySystem* mGuiSys) : dbgOn(showDebug),
																	 ogreMgr(ogreSceneManager),
																	 guiSys(mGuiSys),
																	 mMove(0.5),
																	 turnSpd(Ogre::Degree(1.0)),
																	 transVector(Ogre::Vector3::ZERO),
																	 numObjs(0),
																	 numBoxesFired(0),
																	 controllableCharacter(false),
																	 playerCreated(false),
																	 currPlayerNode(NULL),
																	 currCamNode(NULL)
{
	MyLogger mLog("MyGui Logger");
	guiLog = mLog;
	guiMgr = Ogre::OverlayManager::getSingletonPtr();
	mApp = app;
	//mCurrPlayer = NULL;
};

MyGui::~MyGui()
{

};

void MyGui::addStartScreen(void)
{
	mainScreen = (Ogre::Overlay*)guiMgr->getByName("GuiOverlays/DebugStats");
	mDbgTxt1 = (Ogre::TextAreaOverlayElement*)guiMgr->getByName("BorderPanel/Framerate");

	if(!mainScreen)
	{
		guiLog.logD("Could not find overlay with name: GuiOverlays/DebugStats, in any loaded overlay scripts.");
	}
	else
	{
		mainScreen->show();

		if(mDbgTxt1)
		{
			mDbgTxt1->show();
			mDbgTxt1->setCaption( Ogre::StringConverter::toString(mRenderWind->getLastFPS()) );
		}
	}
};

void MyGui::makeNetControls(void)
{
	int quartX = scnWidth/4;
	int quartY = scnHeight/4;

	netPanel = static_cast<Ogre::OverlayContainer*>(guiMgr->createOverlayElement("Panel","NetControls",true));
	netPanel->setMetricsMode(Ogre::GMM_PIXELS);
	netPanel->setDimensions( quartX, quartY );
	netPanel->setMaterialName( "MyMats/BlueSteel" );
	netPanel->setPosition(quartX*1.5,quartY*3);

	testSendButton = static_cast<Ogre::OverlayContainer*>(guiMgr->createOverlayElement("Panel","NetControlsSend",true));
	testSendButton->setMetricsMode(Ogre::GMM_PIXELS);
	testSendButton->setDimensions(quartX/4,quartY/4);
	testSendButton->setMaterialName("MyMats/ArrowUp");
	testSendButton->setPosition(float(quartX/4*1.5),float(quartY/4*1.4));

	netClient.init();
	netPanel->addChild(testSendButton);
	guiOverlay->add2D(netPanel);

	activeGuiPanels[netPanel] = guiOverlay;
	Vector2 panelPos = Vector2(netPanel->getLeft(),netPanel->getTop());
	panelOriginalPositions[netPanel] = panelPos;
};

void MyGui::setPlayerNode(Ogre::SceneNode* mPlayerNode)
{
	currPlayerNode = mPlayerNode;
	playerCreated = true;
};

Ogre::SceneNode* MyGui::getCurrentPlayerNode()
{
	return (currPlayerNode);
};

void MyGui::init(Ogre::SceneNode* currentCamNode, Ogre::Viewport* currViewport)
{
	guiVP = currViewport;
	mCurrCamera = guiVP->getCamera();
	scnWidth = guiVP->getActualWidth();
	scnHeight = guiVP->getActualHeight();

	originalScreen.screenHeight = scnHeight;
	originalScreen.screenWidth = scnWidth;
	originalScreen.originalOrientation = findCurrentScreenOrientation();
	currentOrientation = originalScreen.originalOrientation;
	//guiSys = OGRE_NEW Ogre::OverlaySystem();
	//ogreMgr->addRenderQueueListener(guiSys);
	//guiMgr = Ogre::OverlayManager::getSingletonPtr();
	guiOverlay = guiMgr->create( "Gui/Main" );
	guiOverlay->show();

	currCamNode = currentCamNode;
	mTimeUntilNextToggle = 0;
	if( dbgOn )
	{
		addDebug();
	}

	isStarted = true;

};

SCREENORIENTATION MyGui::findCurrentScreenOrientation(void)
{
	AConfiguration* config = AConfiguration_new();
	AConfiguration_fromAssetManager(config, mApp->activity->assetManager);
	int32_t orientation = AConfiguration_getOrientation(config);
	SCREENORIENTATION currentScreenOrientation;
	if(orientation == ACONFIGURATION_ORIENTATION_LAND)
	{
		currentScreenOrientation = LANDSCAPE;
	}
	else if(orientation == ACONFIGURATION_ORIENTATION_PORT)
	{
		currentScreenOrientation = PORTRAIT;
	}

	return (currentScreenOrientation);
};


// Landscape to portrait: x*pAspectRatio,y*lAspectRatio
// Portrait to landscape: x*lAspectRatio,y*pAspectRatio
void MyGui::screenResized(SCREENORIENTATION LANDSCAPE_OR_PORTRAIT)
{
	LOGI("^&*^^& Resizing gui panels. ");
	SCREENORIENTATION orientation = LANDSCAPE_OR_PORTRAIT;

	if( orientation == originalScreen.originalOrientation )
	{
		std::map<Ogre::OverlayContainer*,Ogre::Vector2>::iterator sizeIt;
		sizeIt = panelOriginalPositions.begin();
		for (sizeIt=panelOriginalPositions.begin(); sizeIt!=panelOriginalPositions.end(); ++sizeIt)
		{
			sizeIt->first->setPosition(sizeIt->second.x,sizeIt->second.y);
			//float sizeY = sizeIt->first->getHeight();
			//float sizeX = sizeIt->first->getWidth();
			//sizeIt->first->setDimensions(sizeY,sizeX);
		}
	}
	else if( orientation != originalScreen.originalOrientation )
	{
		std::map<Ogre::OverlayContainer*,Ogre::Vector2>::iterator sizeIt;
		sizeIt = panelOriginalPositions.begin();
		for (sizeIt=panelOriginalPositions.begin(); sizeIt!=panelOriginalPositions.end(); ++sizeIt)
		{
			int oldX = sizeIt->second.x;
			int oldY = sizeIt->second.y;
			int oldW = originalScreen.screenWidth;
			int oldH = originalScreen.screenHeight;
			float fX = (float)oldX/oldW;
			float fY =  (float)oldY/oldH;
			int newW = guiVP->getActualWidth();
			int newH = guiVP->getActualHeight();
			float newX = (float)fX*newW;
			float newY = (float)fY*newH;

			std::stringstream ss;
			ss << "\n oldX:" << oldX << " oldY:" << oldY
				<< "\n oldX/oldW-fX:" << fX << " oldY/oldH-fY:" << fY
				<< "\n newX:" << newX << " newY:" << newY;
			LOGI(ss.str().c_str());
			ss.str("");
			ss.clear();

			ss << "\n ScreenWidth:" << mViewport->getActualWidth()
					<< "\n ScreenHeight:" << mViewport->getActualHeight();
			LOGI(ss.str().c_str());
			ss.str("");
			ss.clear();
			ss << "\n oldW:" << oldW << " oldH:" << oldH <<
				  "\n newW:" << newW << "newH:" << newH;
			LOGI(ss.str().c_str());
			ss.str("");
			ss.clear();
			ss << "\n Panel-Width:" << sizeIt->first->getWidth() << "\n Panel-Height:" << sizeIt->first->getHeight();
			LOGI(ss.str().c_str());

			if( (newW-newX) < sizeIt->first->getWidth() )
			{
				int newX2 = newW-sizeIt->first->getWidth();
				newX = newX2;
				ss << "\n Using newX2:" << newX2 << "Panel-Width:" << sizeIt->first->getWidth();
				LOGI(ss.str().c_str());
				ss.str("");
				ss.clear();
			}
			if( ( newH-newY ) < sizeIt->first->getHeight() )
			{
				int newY2 = newH-sizeIt->first->getWidth();
				newY = newY2;
				ss << "Using newY2:" << newY2;
				LOGI(ss.str().c_str());
				ss.str("");
				ss.clear();
			}

				sizeIt->first->setPosition(newX,newY);

		}
	}

	currentOrientation = LANDSCAPE_OR_PORTRAIT;
};

void MyGui::update(Ogre::Real timeSinceLast)
{
	//SCREENORIENTATION screenOrientation = findCurrentScreenOrientation();
	scnWidth = guiVP->getActualWidth();
	scnHeight = guiVP->getActualHeight();

	// toggling fire timer mTimeUntilNextToggle
	if( (mTimeUntilNextToggle-timeSinceLast)<0 )
	{
		mTimeUntilNextToggle = 0;
	}
	else if( mTimeUntilNextToggle>0 )
	{
		mTimeUntilNextToggle -= timeSinceLast;
	}
	else if( mTimeUntilNextToggle<0)
	{
		mTimeUntilNextToggle = 0;
	}
	//guiLog.logD(Ogre::StringConverter::toString(mTimeUntilNextToggle));

	if(dbgOn)
	{
		updateDebug();
	}

//	if(screenOrientation != currentOrientation)
//	{
//		//screenResized(screenOrientation);
//	}
};

void MyGui::addControls(void)
{	// Create a panel

	Ogre::Vector2 panelPos;
	int pHeight = std::abs(scnHeight/4);
	int yPos = scnHeight-pHeight;
	int pWidth = std::abs(scnWidth/4);

	int zWidth = pWidth/5;
	int zHeight = pHeight/5;

	mControlsText = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "Gui/Main/mControlsPanel/mControlsText",true ) );
	mControlsText->setFontName("Halo3");
	mControlsText->setDimensions(1.0f,0.3f);
	mControlsText->setCaption("Controls");
	mControlsText->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

	mControlsPanel = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel",true ) );
	mControlsPanel->setMetricsMode(Ogre::GMM_PIXELS);
	mControlsPanel->setPosition( (scnWidth-pWidth)-6, yPos-6 );
	mControlsPanel->setDimensions( pWidth+6, pHeight+6 );
	mControlsPanel->setMaterialName( "MyMats/BlueSteel" );

	mControlLft = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel/mControlLft",true ) );
	mControlLft->setMetricsMode(Ogre::GMM_PIXELS);
	mControlLft->setDimensions( zWidth, zHeight );
	mControlLft->setMaterialName( "MyMats/ArrowLeft" );

	mControlRgt = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel/mControlRgt",true ) );
	mControlRgt->setMetricsMode(Ogre::GMM_PIXELS);
	mControlRgt->setDimensions( zWidth, zHeight );
	mControlRgt->setMaterialName( "MyMats/ArrowRight" );

	mControlUp = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel/mControlUp",true ) );
	mControlUp->setMetricsMode(Ogre::GMM_PIXELS);
	mControlUp->setDimensions( zWidth, zHeight );
	mControlUp->setMaterialName( "MyMats/ArrowUp" );

	mControlDn = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel/mControlDn",true ) );
	mControlDn->setMetricsMode(Ogre::GMM_PIXELS);
	mControlDn->setDimensions( zWidth, zHeight );
	mControlDn->setMaterialName( "MyMats/ArrowDown" );

	guiOverlay->add2D(mControlsPanel);

	mControlsPanel->addChild(mControlsText);
	mControlsPanel->addChild(mControlLft);
	mControlsPanel->addChild(mControlRgt);
	mControlsPanel->addChild(mControlUp);
	mControlsPanel->addChild(mControlDn);

	//activeGuiPanels.push_back(mControlRgt);
	//activeGuiPanels.push_back(mControlUp);
	//activeGuiPanels.push_back(mControlDn);

	float centerX = ((pWidth+6)-zWidth)/2;
	float centerY = ((pHeight+6)-zWidth)/2;

	mControlsText->setPosition(0.005f,0.005f);
	mControlLft->setPosition( 0.15*pWidth,centerY );
	mControlRgt->setPosition( 0.75*pWidth,centerY );
	mControlUp->setPosition( centerX,0.15*pHeight );
	mControlDn->setPosition( centerX,0.75*pHeight );

	mMakeCharButton = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mMakeCharButton" ) );
	mMakeCharButton->setMetricsMode(Ogre::GMM_PIXELS);
	mMakeCharButton->setPosition( (scnWidth-pWidth)-65, yPos );
	mMakeCharButton->setDimensions( zWidth, pHeight-65 );
	mMakeCharButton->setMaterialName( "MyMats/BlueSteel" );

	guiOverlay->add2D(mMakeCharButton);

	Ogre::Overlay* fireControl = guiMgr->create( "TestFireControls" );
	fireButton1 = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "TestFireControls/FireButton1" ) );
	fireButton1->setMetricsMode(Ogre::GMM_PIXELS);
	fireButton1->setPosition( (scnWidth-(pWidth*2)), yPos );
	fireButton1->setDimensions( zWidth+55, pHeight );
	fireButton1->setMaterialName( "MyMats/Orange" );
	fireButton1->setCaption("FIRE BOX");
	fireControl->add2D(fireButton1);
	fireControl->show();

	guiOverlay->show();

	activeGuiPanels[mControlsPanel] = guiOverlay;
	activeGuiPanels[fireButton1] = fireControl;
	activeGuiPanels[mMakeCharButton] = guiOverlay;

	panelPos = Vector2(mControlsPanel->getLeft(),mControlsPanel->getTop());
	panelOriginalPositions[mControlsPanel] = panelPos;

//	mControls = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "Gui/Main/mControlsPanel/mControls",true ) );
//	mControls->setMetricsMode(Ogre::GMM_PIXELS);
//	mControls->setPosition( 0.3f,0.3f );
//	mControls->setDimensions( pWidth, pHeight );
//	mControls->setMaterialName( "MyMats/Transparent2" );
//  mControlsPanel->addChild(mControls);
};

void MyGui::addDebug()
{	LOGD("*^MyGui::addDebug() started.");
		// Create a panel

		int pHeight = std::abs(scnHeight/4);
		int yPos = scnHeight-pHeight;
		int pWidth = std::abs(scnWidth/4);

		statsPanel = static_cast<Ogre::OverlayContainer*>( guiMgr->createOverlayElement( "Panel", "SceneStats" ) );
		statsPanel->setMetricsMode(Ogre::GMM_PIXELS);
		statsPanel->setPosition( 0, yPos );
		statsPanel->setDimensions( pWidth, pHeight );
		statsPanel->setMaterialName( "MyMats/BlueSteel" );

		// Create text areas
		// Make Average FPS display
		fpsText = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "fpsText" ) );
		fpsText->setFontName("bluehighway-12");
		fpsText->setDimensions(0.3f,0.2f);
		fpsText->setCaption("FPS WILL BE DISPLAYED HERE");
		fpsText->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		// Make Batches text display
		batchesText = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "batchesText" ) );
		batchesText->setFontName("bluehighway-12");
		batchesText->setDimensions(0.5f,0.2f);
		batchesText->setCaption("NUM BATCHES WILL BE DISPLAYED HERE");
		batchesText->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		trianglesText = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "trianglesText" ) );
		trianglesText->setFontName("bluehighway-12");
		trianglesText->setDimensions(1.f,0.2f);
		trianglesText->setCaption("NUM TRIANGLES WILL BE DISPLAYED HERE");
		trianglesText->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		debugText1 = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "debugText1" ) );
		debugText1->setFontName("bluehighway-12");
		debugText1->setDimensions(1.f,0.2f);
		debugText1->setCaption("DEBUGTEXT1");
		debugText1->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		debugText2 = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "debugText2" ) );
		debugText2->setFontName("bluehighway-12");
		debugText2->setDimensions(1.f,0.2f);
		debugText2->setCaption("DEBUGTEXT2");
		debugText2->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		debugText3 = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "debugText3" ) );
		debugText3->setFontName("bluehighway-12");
		debugText3->setDimensions(1.f,0.2f);
		debugText3->setCaption("DEBUGTEXT3");
		debugText3->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		debugText4 = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "debugText4" ) );
		debugText4->setFontName("bluehighway-12");
		debugText4->setDimensions(1.f,0.2f);
		debugText4->setCaption("DEBUGTEXT4");
		debugText4->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		debugText5 = static_cast<TextAreaOverlayElement*>( guiMgr->createOverlayElement( "TextArea", "debugText5" ) );
		debugText5->setFontName("bluehighway-12");
		debugText5->setDimensions(1.f,0.2f);
		debugText5->setCaption("DEBUGTEXT5");
		debugText5->setColour(Ogre::ColourValue(1.0,0.0,0.0,1.0));

		// Add the statsPanel to the overlay
		guiOverlay->add2D(statsPanel);

		// Add batchesText to the statsPanel
		statsPanel->addChild(trianglesText);
		// Add the text area to the statsPanel
		statsPanel->addChild(fpsText);
		// Add batchesText to the statsPanel
		statsPanel->addChild(batchesText);

		// Add debugText1-5 to the statsPanel
		statsPanel->addChild(debugText1);
		statsPanel->addChild(debugText2);
		statsPanel->addChild(debugText3);
		statsPanel->addChild(debugText4);
		statsPanel->addChild(debugText5);

		activeGuiPanels[statsPanel] = guiOverlay;
		statsPanel->getChild("debugText3")->setCaption("testing getting children panels by name.");
		guiMgr->getOverlayElement("debugText4")->setCaption("adfasfdasgdfsgh");
		// Set Position of children of statsPanels, relative to parent statsPanel
		fpsText->setPosition(0.005f,0.005f);
		batchesText->setPosition(0.005f,0.025f);
		trianglesText->setPosition(0.005f,0.045f);
		debugText1->setPosition(0.005f,0.065f);
		debugText2->setPosition(0.005f,0.085f);
		debugText3->setPosition(0.005f,0.105f);
		debugText4->setPosition(0.005f,0.125f);
		debugText5->setPosition(0.005f,0.145f);

		std::stringstream ss;
		ss << "Screen_Height: " << scnHeight;
		debugText1->setCaption(ss.str().c_str());
		ss.clear();
		ss.str("");
		ss << "Screen_Width: " << scnWidth;
		debugText2->setCaption(ss.str().c_str());
		ss.clear();
		ss.str("");
		ss << "pHeight" << pHeight;
		debugText3->setCaption(ss.str().c_str());
		ss.clear();
		ss.str();

		guiOverlay->show();
		guiLog.logI("MyGui::addDebug() finished adding debug panels/stats.");
};

void MyGui::updateDebug()
{
	std::stringstream ss;
	ss << "AVG-FPS: " << mRenderWind->getAverageFPS();
	fpsText->setCaption(ss.str().c_str());

	ss.clear();
	ss.str("");
	ss << "NUM-BATCHES: " << mRenderWind->getBatchCount();
	batchesText->setCaption(ss.str().c_str());

	ss.clear();
	ss.str("");
	ss << "NUM-TRIS: " << mRenderWind->getTriangleCount();
	trianglesText->setCaption(ss.str().c_str());
	ss.clear();
	ss.str("");

	ss << "Screen-Width: " << scnWidth;
	debugText1->setCaption(ss.str().c_str());
	ss.clear();
	ss.str("");
	ss << "Screen-Height: " << scnHeight;
	debugText2->setCaption(ss.str().c_str());
	ss.clear();
	ss.str("");

	if(currPlayerNode!=NULL)
	{
		ss.clear();
		ss.str("");
		Vector3 playerPos = currPlayerNode->getPosition();
		ss << "Current Position: X:" << playerPos.x << " ,Y:" << playerPos.y << " ,Z:" << playerPos.z;
		debugText4->setCaption(ss.str().c_str());
		ss.clear();
		ss.str("");
	}
	else
	{
		debugText4->setCaption("No current player active.");
	}

	if(currCamNode!=NULL)
	{
		ss.clear();
		ss.str("");
		Vector3 camPos = currCamNode->_getDerivedPosition();
		ss << "CurrentPos: X:" << camPos.x << " ,Y:" << camPos.y << " ,Z:" << camPos.z;
		debugText5->setCaption(ss.str().c_str());
		ss.clear();
		ss.str("");
	}
	else
	{
		debugText5->setCaption("No current camera active.");
	}

	ss.clear();
	ss.str("");
};

Ogre::OverlayContainer* MyGui::makeProgressBar(/*percent between 0.0-1.0*/float percent)
{// TODO: CREATE/UPDATE PROGRESS BAR DIMENSIONS/POSITION
 // TODO: DEPENDING ON LANDSCAPE OR PORTRAIT MODE ACTIVE

	Ogre::Overlay* progressBarOverlay;
	Ogre::OverlayContainer* progressBarOverlayBG;
	Ogre::OverlayContainer* progressBarBG;
	Ogre::OverlayContainer* progressBarProgress;

	int screenHeight = mViewport->getActualHeight();
	int screenWidth = mViewport->getActualWidth();
	int progBarWidth = std::abs(screenWidth*.75);
	int progBarHeight = std::abs(screenHeight*.15);
	int progBarPosX = std::abs((screenWidth-progBarWidth)/2);
	int progBarPosY = std::abs((screenHeight-progBarHeight)-25);

	progressBarOverlay = guiMgr->create("progressBarOverlay");//static_cast<Ogre::OverlayContainer*>(mOverlayMgr->
							//			createOverlayElement("Panel","ProgressBarOverlayParent"));;//mOverlayMgr->create( "ProgressBar_Overlay" );

	progressBarOverlayBG = static_cast<Ogre::OverlayContainer*>(guiMgr->
								createOverlayElement("Panel","ProgressBarOverlayBG"));
	progressBarOverlayBG->setMetricsMode(Ogre::GMM_PIXELS);
	progressBarOverlayBG->setDimensions(screenWidth,screenHeight);
	progressBarOverlayBG->setPosition(0,0);
	progressBarOverlayBG->setMaterialName( "MyMats/ProgressBar_Black" );

	progressBarBG = static_cast<Ogre::OverlayContainer*>(guiMgr->
											createOverlayElement("Panel","ProgressBarBG"));
	progressBarBG->setMetricsMode(Ogre::GMM_PIXELS);
	progressBarBG->setDimensions(progBarWidth,progBarHeight);
	progressBarBG->setPosition(progBarPosX,progBarPosY);
	progressBarBG->setMaterialName( "MyMats/ProgressBar_Grey" );


	progressBarProgress = static_cast<Ogre::OverlayContainer*>(guiMgr->
								createOverlayElement("Panel","ProgressBarProgressAmount"));
	progressBarProgress->setMetricsMode(Ogre::GMM_PIXELS);
	progressBarProgress->setDimensions(std::abs(progBarWidth*percent),progBarHeight);
	//progressBarProgress->setPosition(progBarPosX,progBarPosY);
	progressBarProgress->setMaterialName( "MyMats/ProgressBar_Red" );

	activeGuiPanels[progressBarProgress] = progressBarOverlay;

	progressBarOverlay->add2D(progressBarOverlayBG);
	progressBarOverlay->add2D(progressBarBG);
	progressBarBG->addChild(progressBarProgress);
	//progressBarProgress->setPosition(progBarPosX,progBarPosY);
	progressBarProgress->setHorizontalAlignment(GHA_LEFT);
	progressBarProgress->setVerticalAlignment(GVA_TOP);
	progressBarOverlay->show();

	return (progressBarProgress);
};

void MyGui::destroyGui(void)
{

};

void MyGui::destroyProgressBar(Ogre::OverlayContainer* progressBarToDestroy)
{
	Ogre::Overlay* progressBarParentToDestroy = activeGuiPanels[progressBarToDestroy];
	guiMgr->destroy(progressBarParentToDestroy->getName());
	//progressBarToDestroy->getParent()->getParent();//->removeChild(progressBarToDestroy->getName());
	//progressBarToDestroy->getParent()->getParent()->setEnabled(false);
	//mOverlayMgr->destroyOverlayElement(progressBarToDestroy->getParent());
};

bool MyGui::setProgressBarProgress(Ogre::OverlayContainer* progressBarToProgress, float amtToProgress)
{
	Ogre::OverlayContainer* progressBarBG;
	progressBarBG = progressBarToProgress->getParent();

	progressBarToProgress->setDimensions( std::abs(progressBarBG->getWidth()*amtToProgress),
	                    									progressBarBG->getHeight());
	return (true);
};

// ***********  This comes from the OgreSampleBrowser ****************************
// Have to use (Ogre::OverlayContainer*) panel->setMetricsMode(Ogre::GMM_PIXELS);
// or else coords come out in a range of (0.0 -> 1.0) but if we take into account
// current screen we can multiply x(0.0 - 1.0)*ViewWidth; y(0.0 - 1.0)*ViewHeight
bool MyGui::isCursorOver(Ogre::OverlayElement* element, const Ogre::Vector2& cursorPos)//, Ogre::Real voidBorder = 0)
{
	int voidBorder = 0;
	Ogre::Real l = element->_getDerivedLeft() * guiMgr->getViewportWidth();
	Ogre::Real t = element->_getDerivedTop() * guiMgr->getViewportHeight();
	Ogre::Real r = l + element->getWidth();
	Ogre::Real b = t + element->getHeight();
	LOGD("^*^*Element- left , top , right , bottom : left %f / top %f / right %f / bottom %f",l,t,r,b);
	return (cursorPos.x >= l + voidBorder && cursorPos.x <= r - voidBorder &&
			cursorPos.y >= t + voidBorder && cursorPos.y <= b - voidBorder);
}

void MyGui::handlePointerTouchDown(float touchX,float touchY)
{
		//DO MOTION STUFF
		float x = touchX;
		float y = touchY;
		Ogre::OverlayElement* mElement;// = static_cast<Ogre::OverlayElement*>(panel);
		//LOGI("Start of MyGui::handlePointerTouchDown\nReceived touch coords: x=%f,y=%f",touchX,touchY);

//		mElement = static_cast<Ogre::OverlayElement*>(testSendButton);
//		if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
//		{
//			//  If a player object has been created
//			netClient.testSend();
//
//		}

		// Handle click events on mMakeCharButton
		mElement = static_cast<Ogre::OverlayElement*>(mMakeCharButton);
		if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
		{
			//  If a player object has been created
			if(!playerCreated)
			{	LOGI("Creating player.");
				setupCharacter();
				LOGI("setupCharacter finished.");
				ogreMgr->getRootSceneNode()->removeChild(currCamNode);
				currPlayerNode->addChild(currCamNode);
				LOGI("Cam node added to playernode");

				currCamNode->rotate(Vector3(0,1,0), Ogre::Radian(Ogre::Degree(180.0)), Ogre::Node::TS_LOCAL);
				Ogre::Vector3 ctransVector = Ogre::Vector3::ZERO;
				ctransVector.z += 75;
				currCamNode->translate(ctransVector, Ogre::Node::TS_LOCAL);
				LOGI("Player creation complete.");

			}
			else
			{
				//LOGD("Player object has already been created");
				// If a controllable character has been added
				if(controllableCharacter)
				{
					if( mainScreen->isVisible() )
					{
						//mainScreen->hide();
					}
					else
					{
						//mainScreen->show();
					}
					//controllableCharacter = false;
					//mMakeCharButton->setCaption("OFF");
					//mMakeCharButton->setColour(ColourValue(1,0,0,1));

				}
				else
				{
					//controllableCharacter = true;
					//mMakeCharButton->setCaption("ON");
					//mMakeCharButton->setColour(ColourValue(0,1,0,1));
				}
			}

		}


		// Handle player controls
		// TODO: Future: set this up in a player controller class
		if(controllableCharacter)
		{
			Ogre::SceneNode* pNode = ogreMgr->getSceneNode("Sinbad_Node");
		// If mControlUp (arrowUp) has been touched
			mElement = static_cast<Ogre::OverlayElement*>(mControlUp);
			if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
			{
				//LOGD("Moving Sinbad Forward");
				transVector.z += mMove;
				pNode->translate(transVector, Ogre::Node::TS_LOCAL);
			}

			mElement = static_cast<Ogre::OverlayElement*>(mControlLft);
			if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
			{
				//LOGD("Touched left control button!!");
				pNode->rotate( Vector3(0,1,0), turnSpd, Ogre::Node::TS_LOCAL);

			}

			mElement = static_cast<Ogre::OverlayElement*>(mControlRgt);
			if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
			{
				//LOGD("Touched right control button!!");
				pNode->rotate( Vector3(0,1,0), -turnSpd, Ogre::Node::TS_LOCAL);
			}

			mElement = static_cast<Ogre::OverlayElement*>(mControlDn);
			if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
			{
				//LOGD("Moving Sinbad Backwards");
				transVector.z -= mMove;
				pNode->translate(transVector, Ogre::Node::TS_LOCAL);
			}

			mElement = static_cast<Ogre::OverlayElement*>(fireButton1);
			if( isCursorOver( mElement, Ogre::Vector2(x,y) ) )
			{
				//LOGD("Firing box from sinbad!!!");
				fireBox(pNode,pNode->getPosition());
			}

			btTransform updatedTransform;
			Ogre::Vector3 nodePos = pNode->getPosition();
			btVector3 btNodePos;
			// Convert the Ogre::Vector3 to a btVector3
			btNodePos = btVector3(nodePos.x, nodePos.y, nodePos.z);
			updatedTransform.setIdentity();
			updatedTransform.setOrigin(btNodePos);

			std::map<std::string,btRigidBody *>::iterator it = mBodies.begin();
			for (it=mBodies.begin(); it!=mBodies.end(); ++it)
			{
				if( it->first == "Sinbad" )
				{
					static_cast<MyKinematicState*>(it->second->getMotionState())->setKinematicPos(updatedTransform);
				}
			}

			transVector = Ogre::Vector3::ZERO;
		}// ^^** End of if(controllableCharacter) **^^ //

};

void MyGui::setupCharacter()
{
	if(!playerCreated)
	{

		Ogre::Entity* tEntity;
		Ogre::SceneNode* charNode;
		btCollisionShape *mTempShape;
		btDefaultMotionState* mTempState;
		BtOgre::RigidBodyState* mRigidState;
		MyKinematicState* mKinMotionState;
		btRigidBody *mCharBody;

		// Add 'Sinbad' ogre mesh to display
		tEntity = mSceneMgr->createEntity("Sinbad", "Sinbad.mesh");
		tEntity->setCastShadows(true);
		charNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Sinbad_Node");
		charNode->attachObject(tEntity);
		charNode->setPosition(0,0,0);

		if( mSceneMgr->hasEntity("Sinbad") )
		{// make Collision shape, state & body for 'SinbadInstance'
			LOGD("mSceneMgr found 'SinbadInstance' entity");

			tEntity = mSceneMgr->getEntity("Sinbad");
			BtOgre::AnimatedMeshToShapeConverter mConverter1(tEntity);
			mTempShape = mConverter1.createBox();
			btScalar mass = 5;
			btVector3 inertia;
			mTempShape->calculateLocalInertia(mass,inertia);

			btTransform startTransform;
			startTransform.setIdentity();
			startTransform.setOrigin( btVector3(0.0, 0.0, 0.0) );
			mKinMotionState = new MyKinematicState(startTransform,charNode);
			mCharBody = new btRigidBody(mass,mKinMotionState,mTempShape,inertia);
			mCharBody->setFriction(0.5f);
			mCharBody->setRestitution(10.0f);

			mCharBody->setCollisionFlags(mCharBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
			mCharBody->setActivationState(DISABLE_DEACTIVATION);

			mCharBody->activate(true);
			mPhysWorld->addRigidBody(mCharBody);
			mShapes.push_back(mTempShape);
			mRigBodies.push_back(mCharBody);
			mBodies.insert( std::pair<std::string,btRigidBody *>("Sinbad",mCharBody) );

			activeObjects.insert( std::pair<std::string,Ogre::SceneNode*>(tEntity->getName(),charNode) );

			charNode->setPosition(0,5,25);
			currPlayerNode = charNode;
			playerCreated = true;
			controllableCharacter = true;

		}
		else
		{
			LOGD("mSceneMgr did not find 'SinbadInstance' entity\n Player not created.");
		}

	}
	else
	{
		LOGI("Warning: Player was already created with this function.");
	}
};

void MyGui::fireBox(Ogre::SceneNode* nodeRequestingFire, Vector3 startPos)
{
	if( mTimeUntilNextToggle <= 0 )
	{
		numBoxesFired++;

		startPos.z += 3;

		btScalar mass = 75;
		btVector3 inertia;

		Entity *boxEntity;
		btCollisionShape *tmpShape;
		//btBoxShape *boxShape;
		//btConvexHullShape* convexShape;
		btDefaultMotionState* boxState;
		BtOgre::RigidBodyState* mRigidState;
		btRigidBody *boxBody;
		SceneNode *boxNode;

		boxEntity = mSceneMgr->createEntity("FiredBoxs_Box:" + StringConverter::toString(numBoxesFired), "cube.mesh");
		boxEntity->setMaterialName("MyMats/BumpyMetal");

		boxNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("FiredBoxs_boxNode:" + StringConverter::toString(numBoxesFired));
		boxNode->attachObject(boxEntity);
		boxNode->setPosition(startPos);
		boxNode->scale(0.05f,0.05f,0.05f);   // the cube is too big for us

		BtOgre::StaticMeshToShapeConverter mConverter2(boxEntity); //AnimatedMeshToShapeConverter
		tmpShape = mConverter2.createBox();
		tmpShape->calculateLocalInertia(mass,inertia);

		// make a default motion state for the new object
		//boxState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3(0,0,0)) );
		// make a rigidbodystate
		mRigidState = new BtOgre::RigidBodyState(boxNode);
		// new rigidbody for shape, this case a box shape
		boxBody = new btRigidBody(mass,mRigidState,tmpShape,inertia); //btRigidBody(mass,boxState,boxShape,inertia);
		boxBody->setFriction(1.0f);
		boxBody->setRestitution(1.0f);
		boxBody->setDamping(0.1f,0.1f);

		mPhysWorld->addRigidBody(boxBody);
		mShapes.push_back(tmpShape);
		mRigBodies.push_back(boxBody);

		Vector3 startPosition = (nodeRequestingFire->_getDerivedPosition() + mCurrCamera->getDerivedDirection().normalisedCopy() * 10);
		boxNode->setPosition(startPosition);
		btTransform boxTransform;
		Ogre::Quaternion mQuat = nodeRequestingFire->getOrientation();
		btQuaternion rot = btQuaternion(mQuat.x,mQuat.y,mQuat.z,mQuat.w);

		boxTransform.setIdentity();
		boxTransform.setOrigin( btVector3(startPosition.x, startPosition.y, startPosition.z) );
		boxTransform.setRotation(rot);
		boxBody->setWorldTransform(boxTransform);
		Vector3 linV = (mCurrCamera->getDerivedDirection().normalisedCopy() * 150);
		boxBody->setLinearVelocity(btVector3(linV.x,linV.y,linV.z));

		mTimeUntilNextToggle = 0.45f;
	}
};
#endif

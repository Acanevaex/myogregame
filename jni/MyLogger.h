#ifndef __MYLOGGER_H_INCLUDED__
#define __MYLOGGER_H_INCLUDED__

#include <cmath>
#include <EGL/egl.h>
#include <android/log.h>
#include <android/sensor.h>
#include <android_native_app_glue.h>
struct android_app* statApp;
#define OGRE_STATIC_GLES2
#define OGRE_STATIC_ParticleFX
#define OGRE_STATIC_OctreeSceneManager

#include "OgrePlatform.h"
#include "Ogre.h"
#include "OgreOverlaySystem.h"
#include "OgreTextAreaOverlayElement.h"
#include "OgreFontManager.h"
#include "OgreRenderWindow.h"
#include "OgreStringConverter.h"

#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include "Android/OgreAndroidEGLWindow.h"
#include "OgreStaticPluginLoader.h"
#include "RTShaderHelper.h"

#include "OIS.h"

#include "BtOgrePg.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "BulletDynamics/Character/btCharacterControllerInterface.h"

#include "MyMotionStates.h"
#include "android_socket.h"

static AAssetManager* mAssetMgr = NULL;

// ** Ogre declarations ** //
using namespace Ogre;

static Ogre::RenderWindow* mRenderWind = NULL;
static Ogre::Root* mRoot = NULL;
static Ogre::SceneManager* mSceneMgr = NULL;
static Ogre::Camera* mGameCam;
static Ogre::ShaderGeneratorTechniqueResolverListener* mMatListener = NULL;
static Ogre::StaticPluginLoader* mStatPluginLoader = NULL;
static Ogre::OverlayManager* mOverlayMgr = NULL;
static Ogre::OverlaySystem* mOverlaySys = NULL;
static Ogre::Viewport* mViewport = NULL;

// ** Bullet/BtOgre declarations ** //
static float _GRAVITY = -9.8f;
static btDynamicsWorld *mPhysWorld = NULL;
static btAxisSweep3 *mBroadphase = NULL;
static btDefaultCollisionConfiguration *mCollisionConfig = NULL;
static btCollisionDispatcher *mCollisionDispatcher = NULL;
static btSequentialImpulseConstraintSolver *mConstraintSolver = NULL;

std::deque<btCollisionShape *>  mShapes;
std::deque<btRigidBody *> 		mRigBodies;
std::map<std::string,btRigidBody *> mBodies;
std::map<std::string,Ogre::SceneNode*> activeObjects;
static BtOgre::DebugDrawer* mDbgDrw = NULL;


ASensorManager* mSensorMgr;
const ASensor* mAccelSensor;
const ASensor* mGyroSensor;
ASensorEventQueue* mSensorEventQueue;

static Ogre::SceneNode* startTempNode;
static Ogre::SceneNode* currCamNode1;
static Ogre::Entity* startTempEntity;

// OGRE HELPER FUNCTIONS BELOW
//This is from the Ogre3d Android Sample
static Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName)
{
	Ogre::DataStreamPtr stream;
    AAsset* asset = AAssetManager_open(mAssetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
    if(asset)
    {
		off_t length = AAsset_getLength(asset);
        void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
        memcpy(membuf, AAsset_getBuffer(asset), length);
        AAsset_close(asset);

        stream = Ogre::DataStreamPtr(new Ogre::MemoryDataStream(membuf, length, true, true));
    }
    return (stream);
};

void destroyAllAttachedMovableObjects( SceneNode* i_pSceneNode )
{
   if ( !i_pSceneNode )
   {
      return;
   }
   // Destroy all the attached objects
   SceneNode::ObjectIterator itObject = i_pSceneNode->getAttachedObjectIterator();
   while ( itObject.hasMoreElements() )
   {
      //MovableObject* pObject = static_cast<MovableObject*>(itObject.getNext());
      i_pSceneNode->getCreator()->destroyMovableObject( itObject.getNext() );
   }
   // Recurse to child SceneNodes
   SceneNode::ChildNodeIterator itChild = i_pSceneNode->getChildIterator();
   while ( itChild.hasMoreElements() )
   {
      SceneNode* pChildNode = static_cast<SceneNode*>(itChild.getNext());
      destroyAllAttachedMovableObjects( pChildNode );
   }
};

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, "MyOgreGame", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "MyOgreGame", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, "MyOgreGame", __VA_ARGS__)

class MyLogger
{
public:
	MyLogger(){};
	MyLogger(std::string);
	~MyLogger();

	void logI(std::string);
	void logW(std::string);
	void logD(std::string);
	std::string logtag;
};

MyLogger::MyLogger(std::string LOGTAG)//:logtag(LOGTAG.c_str())
{
	std::stringstream ss;
	ss << LOGTAG;//"Logger created for: " << LOGTAG;
	//LOGI(ss.str().c_str());
	logtag = ss.str().c_str();
	ss.str("");
	ss.clear();
	ss << "Logger created for: " << LOGTAG;
	LOGI( ss.str().c_str() );
};

MyLogger::~MyLogger()
{
	std::stringstream ss;
	ss << "Logger destroyed for: " << logtag;
	LOGI( ss.str().c_str() );
};

void MyLogger::logI(std::string logmsg){ __android_log_write(ANDROID_LOG_INFO,logtag.c_str(),logmsg.c_str()); };
void MyLogger::logW(std::string logmsg){ __android_log_write(ANDROID_LOG_WARN,logtag.c_str(),logmsg.c_str()); };
void MyLogger::logD(std::string logmsg){ __android_log_write(ANDROID_LOG_DEBUG,logtag.c_str(),logmsg.c_str()); };

#endif

#ifndef __MYMOTIONSTATES_H_INCLUDED__
#define __MYMOTIONSTATES_H_INCLUDED__

#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, "MyOgreGame", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "MyOgreGame", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, "MyOgreGame", __VA_ARGS__)
#include "BulletDynamics/Character/btKinematicCharacterController.h"

using namespace Ogre;

//^^**********   Possibly pass in a whole new data type to be able to update internally
//^^********** Edit: Bullet allows passing a custom user data pointer to be able to use inside motionstate?

class MyKinematicState : public btDefaultMotionState
{
	protected:
		bool clampToTerrain;
		SceneNode * mVisibleObj;
		btTransform mTransform;
		btTransform mCOM;

	public:
		MyKinematicState(const btTransform & initialPos, SceneNode * node)
		{
			mVisibleObj = node;
			mTransform = initialPos;
			mCOM = btTransform::getIdentity();
		}

		MyKinematicState(const btTransform & initialPos)
		{
			mVisibleObj = NULL;
			mTransform = initialPos;
		}

		~MyKinematicState() {}

		void setNode(SceneNode * node)
		{
			mVisibleObj = node;
		}

		btTransform getWorldTransform() const
		{
			return (mCOM.inverse() * mTransform);
		}

		// synchronizes world transform from user to physics
		void getWorldTransform(btTransform & worldTrans) const
		{
			worldTrans = mCOM.inverse() * mTransform;
		}

		// synchronizes world transform from physics to user
		// Bullet only calls the update of worldtransform for active objects
		void setWorldTransform(const btTransform & worldTrans)
		{
			if (mVisibleObj == NULL)
				LOGI("MyKinematicMotionState: mVisibleObj == NULL");
				return;
		}

		void setKinematicPos(btTransform &currentPos)
		{
			Ogre::Quaternion mQuat = mVisibleObj->getOrientation();
			btQuaternion rot = btQuaternion(mQuat.x,mQuat.y,mQuat.z,mQuat.w);
			mTransform = currentPos;
			mTransform.setRotation(rot);
		};

		void CheckClamping(void);
};

//^^**       My Character Controller Class   **^^//
class MyCC //: public btKinematicCharacterController
{
	public:
		btDynamicsWorld *physWorldToAttachTo;
		MyKinematicState* playerKinState;
		btRigidBody* playerPhysBody;
		btCollisionShape* playerPhysShape;
		btScalar playerMass;
		btVector3 playerStartPos;

		btPairCachingGhostObject* playerGhostObject;
		btConvexShape* mConvexShape;

		Ogre::SceneNode* playerParentNode;
		Ogre::SceneNode* playerNode;
		Ogre::String playerName;
		int numEntities;

		MyCC(){ numEntities = 0; }; // btPairCachingGhostObject* ghostObject,btConvexShape* convexShape,btScalar stepHeight, int upAxis = 1
		~MyCC(){};

		void setPlayerNode(Ogre::SceneNode* setCharControllerToThisNode);
		bool setPlayerToNewParentNode(Ogre::SceneNode* newParentNodeToAttachTo);
		bool initPlayerPhysics(btVector3 startPos, bool setActive, btDynamicsWorld *physWorldToAddPlayerTo);
		bool initMotionState(void);
		bool initPlayerPhysShape(void);
		bool initPlayerPhysBody(btVector3 playerInertia);

// TODO: Add call to this function inside androids motion event function
		bool movePlayerForward(void);
		bool movePlayerBackward(void);
};

void MyCC::setPlayerNode(Ogre::SceneNode* nodeToMakePlayer)
{
	playerNode = nodeToMakePlayer;
	playerName = playerNode->getName();
	numEntities = playerNode->numChildren();
};

bool MyCC::setPlayerToNewParentNode(Ogre::SceneNode* newParentNode)
{
	if( playerNode->getParentSceneNode()->detachObject(playerName.c_str()) ==
													playerNode->getAttachedObject(playerName.c_str()) )
	{
		newParentNode->addChild(playerNode);
		return (true);
	}
	else
	{
// TODO: Add popup message to let player know something went wrong
		return (false);
	}
};

bool MyCC::initPlayerPhysics(btVector3 startPos, bool setActive,  btDynamicsWorld *physWorldToAddPlayerTo)
{
	physWorldToAttachTo = physWorldToAddPlayerTo;
	playerStartPos = startPos;
	if(setActive)
	{
		if(initMotionState())
		{
			if( initPlayerPhysShape() )
			{
				physWorldToAttachTo->addRigidBody(playerPhysBody);
				return (true);
			}
			else
			{
				return (false);
			}
		}
		else
		{
			return (false);
		}
	}
	else
	{
		if(initMotionState())
		{
			if( initPlayerPhysShape() )
			{
				return (true);
			}
			else
			{
				return (false);
			}
		}
		else
		{
			return (false);
		}
	}
};

bool MyCC::initMotionState()
{
	btTransform startTransform;
	startTransform.setIdentity();
	startTransform.setOrigin(playerStartPos);
	playerKinState = new MyKinematicState(startTransform,playerNode);
	return (true);

};

bool MyCC::initPlayerPhysShape()
{
//	if( mSceneMgr->hasEntity( playerName.c_str()) )
//	{
//		btVector3 inertia;
//		Ogre::Entity* tEntity;
//		tEntity = mSceneMgr->getEntity( playerName.c_str() );
//		BtOgre::AnimatedMeshToShapeConverter mConverter1(tEntity);
//		playerPhysShape = mConverter1.createBox();//.createCapsule();//.createBox();//mConverter1.createConvex();
//		playerPhysShape->calculateLocalInertia(playerMass,inertia);
//
//		if( initPlayerPhysBody(inertia) )
//		{
//			return (true);
//		}
//		else
//		{
//			return (false);
//		}
//	}
//	else
//	{
//		return (false);
//	}
	return (true);
};

bool MyCC::initPlayerPhysBody( 	btVector3 playerInertia )
{
	playerPhysBody = new btRigidBody(playerMass,playerKinState,playerPhysShape,playerInertia);
	playerPhysBody->setFriction(1.0f);
	playerPhysBody->setRestitution(1.0f);
	playerPhysBody->setCollisionFlags(playerPhysBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
	playerPhysBody->setActivationState(DISABLE_DEACTIVATION);
	return (true);
};

#endif

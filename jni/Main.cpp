#include "Game_inline.h"
MyGame* gameListener;

int numObjs = 0;

static bool init(void)
{//LOGD("START OF INIT");

	Ogre::ConfigFile cf;
	cf.load(openAPKFile("resources.cfg"));

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	Ogre::String sec, type, arch;
	while(seci.hasMoreElements())
	{
		sec = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;

		for(i = settings->begin(); i != settings->end(); i++)
		{
			type = i->first;
			arch = i ->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(arch,type,sec);
		}
	}
	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingletonPtr()->setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("General");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("Fonts");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("GUI");
	Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("PACKS");

	//Ogre::ResourceGroupManager::getSingletonPtr()->initialiseAllResourceGroups();


	if( Ogre::RTShader::ShaderGenerator::initialize() )
	{//LOGD("INIT:: Initialized RTShader::ShaderGen.");
		// ONLY FOR GLES2
		Ogre::RTShader::ShaderGenerator::getSingletonPtr()->setTargetLanguage("glsles");
		mMatListener = new Ogre::ShaderGeneratorTechniqueResolverListener();
		Ogre::MaterialManager::getSingleton().addListener(mMatListener);
		LOGD("Init successful.");
		return (true);
	}
	else
	{
		LOGD("INIT:: Failed to Initialize RTShader::ShaderGen.");
		return (false);
	}
}

static void initSceneMgr(void)
{//LOGD("initSceneMgr() started.");

	mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC,"MainSceneManager");
	Ogre::RTShader::ShaderGenerator::getSingletonPtr()->addSceneManager(mSceneMgr);
	mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED);

	LOGD("initSceneMgr() finished.");
}

static void setupView(void)
{// Create camera and viewport

	mGameCam = mSceneMgr->createCamera("mGameCam");
	mGameCam->setNearClipDistance(0.1f);
	mGameCam->setFarClipDistance(3000.0f);
	mGameCam->setAutoAspectRatio(true);
	mViewport = mRenderWind->addViewport(mGameCam);

	//mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0,.10));
	mViewport->setOverlaysEnabled(true);
	mViewport->setMaterialScheme(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
	Ogre::RTShader::ShaderGenerator::getSingletonPtr()->invalidateScheme(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
	mViewport->setShadowsEnabled(true);

	// Create a sceneNode for camera named: CameraNode
	currCamNode1 = mSceneMgr->getRootSceneNode()->createChildSceneNode("CameraNode");
	currCamNode1->attachObject(mGameCam);
	currCamNode1->setPosition(0.0f,4.0f,30.0f);
	mGameCam->lookAt(0.f,2.5f,0.f);

	Ogre::SceneNode* pNode;
	Ogre::Light* pDirectionLight;
	// Create direction light for the scene, at 30u up Y, 25u forward Z, aimed down Z, node name in root Light_Node-1
	pDirectionLight = mSceneMgr->createLight();
	//pDirectionLight->setPowerScale(15);
	//pDirectionLight->setDirection(Ogre::Vector3(0,-1,0));
	pDirectionLight->setType(Ogre::Light::LT_POINT);
	pNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Light_Node-1");
	pNode->attachObject(pDirectionLight);
	pNode->setPosition(0.0,20.0f,0.0);

	startTempEntity = mSceneMgr->createEntity("TempStartEntity", "cube.mesh");
	startTempNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("TempStartNode");
	startTempNode->attachObject(startTempEntity);
	startTempNode->setPosition(0.f,0.f,0.f);
}


static void createBox(Vector3 boxSize,Vector3 startPos)
{
//^^*** Setup new boxes for character to run into for testing

	numObjs++;

	btScalar mass = 5;
	btVector3 inertia;

	Entity *boxEntity;
	btCollisionShape *tmpShape;
	//btBoxShape *boxShape;
	//btConvexHullShape* convexShape;
	btDefaultMotionState* boxState;
	BtOgre::RigidBodyState* mRigidState;
	btRigidBody *boxBody;
	SceneNode *boxNode;

	boxEntity = mSceneMgr->createEntity("Box" + StringConverter::toString(numObjs), "cube.mesh");
	boxEntity->setMaterialName("MyMats/BumpyMetal");
	boxEntity->setCastShadows(true);

	boxNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("boxNode" + StringConverter::toString(numObjs));
	boxNode->attachObject(boxEntity);
	boxNode->setPosition(startPos);
	boxNode->scale(0.05f,0.05f,0.05f);   // the cube is too big for us

	BtOgre::StaticMeshToShapeConverter mConverter2(boxEntity); //AnimatedMeshToShapeConverter
	tmpShape = mConverter2.createBox();
	tmpShape->calculateLocalInertia(mass,inertia);

// make a default motion state for the new object
//boxState = new btDefaultMotionState( btTransform(btQuaternion(0,0,0,1), btVector3(0,0,0)) );
// make a rigidbodystate
	mRigidState = new BtOgre::RigidBodyState(boxNode);
// new rigidbody for shape, this case a box shape
	boxBody = new btRigidBody(mass,mRigidState,tmpShape,inertia); //btRigidBody(mass,boxState,boxShape,inertia);

	boxBody->setFriction(0.5f);
	boxBody->setRestitution(5.0f);
	boxBody->setDamping(0.05f,0.05f);

	boxNode->setPosition(startPos);

	mPhysWorld->addRigidBody(boxBody);
	mShapes.push_back(tmpShape);
	mRigBodies.push_back(boxBody);
};

static void setupScene(void)
{	LOGD("SCENESETUP: START");

	if(startTempNode) 	// TODO: LATER ADD FUNCTION/SETUP TO CLEAR SCENE AND EACH NEW SCENE
	{					// TODO: SETS UP NEW LIGHTS AND OBJECTS
		destroyAllAttachedMovableObjects(startTempNode);
		startTempNode->removeAndDestroyAllChildren();
		mSceneMgr->destroySceneNode(startTempNode);
	}
	Ogre::Entity* tEntity;
	Ogre::SceneNode* pNode;

	// Create plane mesh, name: groundPlane, in ResGroup General.
	Ogre::MeshPtr mMesh = Ogre::MeshManager::getSingleton().createPlane("groundPlane","General",Plane(Vector3::UNIT_Y,0),500,500,
				5, 5, true, 1, 5, 5, Vector3::UNIT_Z);
	mMesh->buildEdgeList();
	tEntity = mSceneMgr->createEntity("Ground", "groundPlane");
	//LOGD("Ground Made.");
	Ogre::MaterialPtr mMat = Ogre::MaterialManager::getSingleton().getByName("MyMats/BumpyMetal");
	mMat->setAmbient(0.5f,0.5f,0.5f);
	mMat->setDiffuse(0.5f,0.5f,0.5f,1);
	mMat->setLightingEnabled(true);
	//LOGD("material properties set.");
	tEntity->setMaterialName("MyMats/BumpyMetal");
	//LOGD("Ground Material Set");
	pNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("groundNode");
	pNode->attachObject(tEntity);
	pNode->setScale(1.f,0.05f,1.f);
	pNode->setPosition(0,0,0);

	mSceneMgr->setSkyDome(true, "MyMats/CloudySky", 8, 8, 1000);

	Vector3 boxSize = Vector3(1,1,1);
	Vector3 startPos1 = Vector3(15,1,-25);
	Vector3 startPos2 = Vector3(-5,1,-5);

	createBox(boxSize,startPos1);
	createBox(boxSize,startPos2);


	LOGD("SCENESETUP: DONE!");
}; //^^*** End of sceneSetup();

static bool handleMotionEvent(struct android_app* app, AInputEvent* event)
{
	//LOGD("*&&&%%%***   MOTION EVENT LOGGED   *&&&^^^%***");
	size_t numEvents;
	int32_t action = AMotionEvent_getAction(event);

	switch(action)
	{
		case AMOTION_EVENT_ACTION_DOWN:
		{	//LOGI("AMOTION_EVENT_ACTION_DOWN");
			float y = AMotionEvent_getRawY(event, 0);
			float x = AMotionEvent_getRawX(event, 0);
			mGui->handlePointerTouchDown(x,y);
			break;
		}
		case AMOTION_EVENT_ACTION_UP:
		{	//LOGI("AMOTION_EVENT_ACTION_UP");
			break;
		}
		case AMOTION_EVENT_ACTION_MOVE:
		{
			int index = (action & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK)
			    						  >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
			int pid = AMotionEvent_getPointerId(event, index);
			float x = AMotionEvent_getX(event, index);
			float y = AMotionEvent_getY(event, index);
			float pressure = AMotionEvent_getPressure(event, index);
			//LOGI("TOUCHPOINT-X: %f | TOUCHPOINT-Y: %f",x,y);
			mGui->handlePointerTouchDown(x,y);
			break;
		}
		case AMOTION_EVENT_ACTION_CANCEL:
		{	LOGI("AMOTION_EVENT_ACTION_CANCEL");
			break;
		}
		case AMOTION_EVENT_ACTION_OUTSIDE:
		{	LOGI("AMOTION_EVENT_ACTION_OUTSIDE");
			break;
		}
		case AMOTION_EVENT_ACTION_POINTER_DOWN:
		{	LOGI("AMOTION_EVENT_ACTION_POINTER_DOWN");
			break;
		}
		case AMOTION_EVENT_ACTION_POINTER_UP:
		{	LOGI("AMOTION_EVENT_ACTION_POINTER_UP");
			break;
		}
		default:
		{	LOGI("DEFAULT");
			break;
		}
	}

	return (true);
};

static int32_t handleInput(struct android_app* app, AInputEvent* event)
{	//LOGD("*************HANDLING INPUT**********");
	switch( AInputEvent_getType(event) )
	{
		case AINPUT_EVENT_TYPE_MOTION://AMOTION_EVENT_ACTION_DOWN:
		{
			handleMotionEvent(app,event);
			break;
		}
		case AINPUT_EVENT_TYPE_KEY:
		{	//DO KEYBOARD STUFF

			int32_t key_val = AKeyEvent_getKeyCode(event);
			LOGD("Received key event: KEY: %d\n", key_val);

			if((key_val >= AKEYCODE_A && key_val <= AKEYCODE_Z))
			{// TODO: make a switch(key_val){ case AKEYCODE_A:{ break;} }   etc etc.....
			    LOGI("Got a letter");
			}

			if(key_val == AKEYCODE_BACK)
			{
				LOGD("BACK BUTTON PRESSED");
			}

			break;
		}
		default:
		{	LOGI("AINPUT_EVENT_TYPE: default??");
			break;
		}
	}

	return (true);
}

static void handleCmd(struct android_app* app, int32_t cmd)
{
    switch (cmd)
    {
        case APP_CMD_SAVE_STATE:
        {
            break;
        }
        case APP_CMD_INIT_WINDOW:
        {
            if(app->window && mRoot)
            {
            	AConfiguration* config = AConfiguration_new();
            	AConfiguration_fromAssetManager(config, app->activity->assetManager);
            	mAssetMgr = app->activity->assetManager;
            	LOGD("INIT:: mAssetMgr created.");

            	//  Start Essential Ogre stuff, this gets called after mRoot was created down in android_main()
                if(!mRenderWind)
                {
                	Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKFileSystemArchiveFactory(app->activity->assetManager) );
                	Ogre::ArchiveManager::getSingleton().addArchiveFactory( new Ogre::APKZipArchiveFactory(app->activity->assetManager) );
                	//LOGD("INIT:: APKZIP AND ZPKFILE factories created.");

                	Ogre::NameValuePairList opt;
                	opt["externalWindowHandle"] = Ogre::StringConverter::toString((int)app->window);
                	opt["androidConfig"] = Ogre::StringConverter::toString((int)config);
                	//LOGD("INIT::  Ogre::NameValuePairList opt declared and set.");

                	mRenderWind = mRoot->createRenderWindow("MyOgreWindow", 0, 0, false, &opt);
                	//mRenderWind->setVSyncEnabled(true);
                	//LOGD("VSYNC Activated.");
                	//LOGD("mRenderWind created.");

                    if( init() ) /* if init of resources and RtShader go alright  */
                    {
                    	initSceneMgr(); // Create SceneManager
                    	setupView();	// Create Camera and Viewport

                    	mGui = new MyGui(app,true,mSceneMgr,mOverlaySys);
                    	//mGui = gui;
                    	mGui->init(currCamNode1,mViewport);

                    	initBullet();			// Create physics world and phys debug drawer
                    	setupScene();			// Create Sinbad and a ground for him to fall on
                    	myInit();				// Create Sinbad's and ground's physics shapes
                    	mGui->addControls();		// Create control pad to control Sinbad

                    	gameListener = new MyGame(mRenderWind,mPhysWorld,mDbgDrw,mGui);
                    	mRoot->addFrameListener(gameListener);

                    	// Testing premade overlay scripts
                    	mGui->addStartScreen();
                    }
                }
                else
                {
					static_cast<Ogre::AndroidEGLWindow*>(mRenderWind)->_createInternalResources(app->window, config);
                }
                AConfiguration_delete(config);
            }
            break;
        }
        case APP_CMD_TERM_WINDOW:
        {
        	LOGD("APP_CMD_TERM_WINDOW");
            if(mRoot && mRenderWind)
				static_cast<Ogre::AndroidEGLWindow*>(mRenderWind)->_destroyInternalResources();
            break;
        }
        case APP_CMD_GAINED_FOCUS:
        {	LOGI("APP_CMD_GAINED_FOCUS");
        // When our app gains focus, we start monitoring the accelerometer.
        	if (mAccelSensor != NULL)
        	{
        		ASensorEventQueue_enableSensor(mSensorEventQueue,mAccelSensor);
        // We'd like to get 60 events per second (this is only approximate).
        		ASensorEventQueue_setEventRate(mSensorEventQueue,mAccelSensor, (1000L/60)*1000);
        	}

            break;
        }
        case APP_CMD_LOST_FOCUS:
        {	LOGI("APP_CMD_LOST_FOCUS");
        // When our app loses focus, we stop monitoring the accelerometer.
        // This is to avoid consuming battery while not being used.
        	if (mAccelSensor != NULL)
        	{
        		ASensorEventQueue_disableSensor(mSensorEventQueue,mAccelSensor);
        	}
            break;
        }
    }
}

//static void handleAccelerometerMovement(float x, float y, float z)
//{
//
//};

AndroidGame* AndroidBridge::mGame = NULL;
AndroidMultiTouch* AndroidBridge::mTouch = NULL;
AndroidKeyboard* AndroidBridge::mKeyboard = NULL;
AndroidInputInjector* AndroidBridge::mInputHandler = NULL;
Ogre::StaticPluginLoader* AndroidBridge::mStaticPluginLoader = NULL;
Ogre::RenderWindow* AndroidBridge::mRenderWnd = NULL;
Ogre::Root* AndroidBridge::gRoot = NULL;
Ogre::OverlaySystem* AndroidBridge::mOverlaySystem = NULL;
bool AndroidBridge::mInit = false;

void android_main(struct android_app* app)
{	app_dummy();	// !IMPORTANT! call app_dummy() from native_glue to make sure the android/native glue code isnt stripped. !IMPORTANT!
	LOGD("APP STARTED");
	//Ogre::Timer* timer = OGRE_NEW Ogre::Timer();

	AndroidBridge::init(app);
	AndroidBridge::go(app);

}
//	mRoot = new Ogre::Root();
//	mStatPluginLoader = new Ogre::StaticPluginLoader();
//	mStatPluginLoader->load();
//	mRoot->setRenderSystem(mRoot->getAvailableRenderers().at(0));
//	mRoot->initialise(false);
//	mOverlaySys = OGRE_NEW Ogre::OverlaySystem();
//	statApp = app;
//	app->onAppCmd = &handleCmd;
//	app->onInputEvent = &handleInput;
//
//	mSensorMgr = ASensorManager_getInstance();
//	mGyroSensor = ASensorManager_getDefaultSensor(mSensorMgr,ASENSOR_TYPE_GYROSCOPE);
//	mAccelSensor = ASensorManager_getDefaultSensor(mSensorMgr,ASENSOR_TYPE_ACCELEROMETER);
//
//	mSensorEventQueue = ASensorManager_createEventQueue(mSensorMgr,
//	            				app->looper, LOOPER_ID_USER, NULL, NULL);
//
//// TODO: NativeActivity sample shows simple engine structure:
//// TODO: if (state->savedState != NULL) {
//// TODO: // We are starting with a previous saved state; restore from it.
//// TODO: engine.state = *(struct saved_state*)state->savedState;
//
//	//LOGD("SET: onAppCmd & onInputEvent to &handleCmd and &handleInput respectively");
//
//	while(true)
//	{
//		int ident, events;
//		struct android_poll_source* source;
//
//		while ((ident = ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0)
//		{
//			if (source != NULL)
//			{
//				source->process(app, source);
//			}
//
//
//			// If a sensor has data, process it now.
//			if (ident == LOOPER_ID_USER)
//			{// TODO: Create function to handle these events in separate area...
//				if (mAccelSensor != NULL)
//				{
//					ASensorEvent event;
//
//					while (ASensorEventQueue_getEvents(mSensorEventQueue,&event, 1) > 0)
//					{
//						handleAccelerometerMovement(event.acceleration.x,event.acceleration.y,event.acceleration.z);
//						//LOGI("accelerometer: x=%f y=%f z=%f",event.acceleration.x, event.acceleration.y,event.acceleration.z);
//					}
//				}
//			}
//
//			if(mGyroSensor != NULL)
//			{
//				//LOGI("^*^* Found gyroscope on device!");
//			}
//
//			if (app->destroyRequested != 0)
//			{
//				//destroyBullet();
//				return;
//			}
//		}
//
//		if(mRenderWind != NULL && mRenderWind->isActive())
//		{ //^****---- THE RUNNING LOOP ----****^//
//			mRenderWind->windowMovedOrResized();
//			mRoot->renderOneFrame();
//
//		}
//	}
//
//	LOGI("MyOgreGame shutting down.");
//	//destroyBullet();
//}
